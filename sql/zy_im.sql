/*
 Navicat Premium Data Transfer

 Source Server         : my
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 8.140.177.170:3306
 Source Schema         : zy_im

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 21/12/2021 14:19:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for im_app
-- ----------------------------
DROP TABLE IF EXISTS `im_app`;
CREATE TABLE `im_app` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT 'app名称',
  `app_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT 'app代码',
  `salt` varchar(24) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '盐',
  `access_key` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '存取键',
  `secret_key` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密钥',
  `on_line_num` int NOT NULL DEFAULT '0' COMMENT '在线人数',
  `total_num` int NOT NULL DEFAULT '0' COMMENT 'app总人数',
  `status` int NOT NULL DEFAULT '0' COMMENT '状态 0-无效 1-有效 2-删除',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for im_message
-- ----------------------------
DROP TABLE IF EXISTS `im_message`;
CREATE TABLE `im_message` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `app_code` varchar(64) NOT NULL DEFAULT '' COMMENT 'app代码',
  `uuid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT 'uuid',
  `session_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '会话代码',
  `from_user_id` bigint NOT NULL DEFAULT '0' COMMENT '发送方',
  `to_user_id` bigint NOT NULL DEFAULT '0' COMMENT '接收方',
  `msg_type` int NOT NULL DEFAULT '0' COMMENT '1-文字 2-图片 3-语音 4-静态表情 5-动态表情 6-外链 7-视频',
  `msg_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '消息内容',
  `status` int NOT NULL DEFAULT '0' COMMENT '状态 -1已撤回 0-未送达  1-已送达  2-已读',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for im_session
-- ----------------------------
DROP TABLE IF EXISTS `im_session`;
CREATE TABLE `im_session` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `app_code` varchar(64) NOT NULL DEFAULT '' COMMENT 'app代码',
  `session_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '会话代码',
  `origin_user_id` bigint NOT NULL DEFAULT '0' COMMENT '发起者',
  `partic_user_id` text NOT NULL COMMENT '参与者  多个逗号分隔',
  `type` int NOT NULL DEFAULT '0' COMMENT '会话类型：1-一对一  2-群聊',
  `status` int NOT NULL DEFAULT '0' COMMENT '状态 0-无效 1-有效 2-删除',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for im_user
-- ----------------------------
DROP TABLE IF EXISTS `im_user`;
CREATE TABLE `im_user` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `app_user_id` bigint NOT NULL COMMENT 'app内useriD',
  `nickname` varchar(64) NOT NULL DEFAULT '' COMMENT '昵称',
  `mobile` varchar(32) NOT NULL DEFAULT '' COMMENT '手机号',
  `salt` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '盐',
  `access_key` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '存取键',
  `secret_key` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密钥',
  `app_id` bigint NOT NULL COMMENT 'appId',
  `app_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT 'app代码',
  `is_on_line` int NOT NULL DEFAULT '0' COMMENT '是否在线  0-不在 1-在',
  `status` int NOT NULL DEFAULT '0' COMMENT '状态 0-无效 1-有效 2-删除',
  `os` varchar(16) NOT NULL DEFAULT '' COMMENT '终端类型  1-安卓 2-ios 3-h5',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10014 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for im_user_session_relation
-- ----------------------------
DROP TABLE IF EXISTS `im_user_session_relation`;
CREATE TABLE `im_user_session_relation` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint NOT NULL DEFAULT '0' COMMENT '用户ID',
  `session_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '会话代码',
  `first_msg_time` timestamp NULL DEFAULT NULL COMMENT '第一条消息时间',
  `last_msg_time` timestamp NULL DEFAULT NULL COMMENT '最后一条消息时间',
  `status` int NOT NULL DEFAULT '0' COMMENT '状态 0-无效 1-有效 2-删除',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

SET FOREIGN_KEY_CHECKS = 1;
