package com.wqnmdb.im.shiro;

import lombok.Data;
import org.apache.shiro.authc.AuthenticationToken;

@Data
public class ShiroToken implements AuthenticationToken {

    private String token;

    private String accessKey;

    private String secretKey;

    private String type;

    public ShiroToken(String token, String type) {
        this.token = token;
        this.type = type;
    }

    public ShiroToken(String accessKey, String secretKey, String type) {
        this.accessKey = accessKey;
        this.secretKey = secretKey;
        this.type = type;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
