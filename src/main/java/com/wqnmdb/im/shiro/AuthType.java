package com.wqnmdb.im.shiro;

/**
 * 认证类型
 */
public enum AuthType {
    SYS_USER("sysUser"),
    APP_USER("appUser");

    private String type;

    private AuthType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return this.type.toString();
    }
}
