package com.wqnmdb.im.shiro;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.pam.ModularRealmAuthenticator;
import org.apache.shiro.authz.Authorizer;
import org.apache.shiro.authz.ModularRealmAuthorizer;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.ArrayList;
import java.util.Collection;

@Slf4j
public class CustomModularRealmAuthrizer extends ModularRealmAuthorizer {

    /**
     * 根据用户类型判断使用哪个Realm
     */
    @Override
    public boolean isPermitted(PrincipalCollection principals, String permission) {
        assertRealmsConfigured();
        for (Realm realm : getRealms()) {
            log.info("realm Name:{}", realm.getName());
            if (!(realm instanceof Authorizer)) {
                continue;
            }
            if (realm.getName().contains(AuthType.SYS_USER.toString())) {
                return ((SysTokenRealm) realm).isPermitted(principals, permission);
            }
            if (realm.getName().contains(AuthType.APP_USER.toString())) {
                return ((UserTokenRealm) realm).isPermitted(principals, permission);
            }
        }
        return false;
    }

    @Override
    public boolean hasRole(PrincipalCollection principals, String roleIdentifier) {
        assertRealmsConfigured();
        // 所有Realm
        Collection<Realm> realms = getRealms();
        for (Realm realm : realms) {
            log.info("realm Name:{}", realm.getName());
            if (!(realm instanceof Authorizer)) {
                continue;
            }
            if (realm.getName().contains(AuthType.SYS_USER.toString())) {
                return ((SysTokenRealm) realm).hasRole(principals, roleIdentifier);
            }
            if (realm.getName().contains(AuthType.APP_USER.toString())) {
                return ((SysTokenRealm) realm).hasRole(principals, roleIdentifier);
            }
        }
        return false;
    }
}
