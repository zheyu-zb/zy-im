package com.wqnmdb.im.shiro;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.TableField;
import com.wqnmdb.im.domain.contants.ResponseEnum;
import com.wqnmdb.im.domain.contants.SystemConstant;
import com.wqnmdb.im.utils.result.R;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class ZYTokenFilter extends AuthenticatingFilter {

    /**
     * 创建Token, 支持自定义Token
     */
    @Override
    protected AuthenticationToken createToken(ServletRequest servletRequest, ServletResponse servletResponse) throws Exception {
        final UserAuthInfo authInfo = this.getToken((HttpServletRequest) servletRequest);
        if (StrUtil.isNotBlank(authInfo.getToken())){
            return new ShiroToken(authInfo.getToken(), AuthType.SYS_USER.toString());
        }else {
            return new ShiroToken(authInfo.getAccessKey(), authInfo.getSecretKey(), AuthType.APP_USER.toString());
        }
    }

    /**
     * 兼容跨域
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        return ((HttpServletRequest) request).getMethod().equals(RequestMethod.OPTIONS.name());
    }

    @Override
    protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) throws Exception {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        UserAuthInfo authInfo = this.getToken(request);
        if (StrUtil.isBlank(authInfo.getToken()) &&
                (StrUtil.isBlank(authInfo.getAccessKey()) || StrUtil.isBlank(authInfo.getSecretKey()))) {
            this.responseException(request, response, "缺少认证信息");
            return false;
        }

        final boolean bool = executeLogin(servletRequest, servletResponse);
        if (!bool){
            this.responseException(request, response, "令牌无效");
        }
        return bool;
    }

    /**
     * 获取 ak sk
     * 优先从header获取
     * 如果没有，则从parameter获取
     *
     * @param request request
     * @return token
     */
    private UserAuthInfo getToken(HttpServletRequest request) {
        String token = request.getHeader(SystemConstant.AUTH_HEAD_NAME);
        String ak = request.getHeader(SystemConstant.AK_HEAD_NAME);
        String sk = request.getHeader(SystemConstant.SK_HEAD_NAME);
        UserAuthInfo asKey = new UserAuthInfo(ak, sk, token);
        return asKey;
    }

    /**
     * 将非法请求跳转到 /unauthorized/**
     */
    private void responseException(HttpServletRequest request, HttpServletResponse response, String message) throws IOException {
        response.setContentType("application/json;charset=utf-8");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));

        R r = new R(ResponseEnum.NOT_LOG_IN.getCode(), ResponseEnum.NOT_LOG_IN.getContent());
        response.getWriter().print(JSONUtil.toJsonStr(r));
    }

    @Data
    class UserAuthInfo{
        private String accessKey;
        private String secretKey;
        private String token;

        public UserAuthInfo(String accessKey, String secretKey, String token) {
            this.accessKey = accessKey;
            this.secretKey = secretKey;
            this.token = token;
        }
    }
}
