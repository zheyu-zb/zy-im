package com.wqnmdb.im.shiro;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.wqnmdb.im.domain.contants.SysRoleEnum;
import com.wqnmdb.im.domain.entity.SysUser;
import com.wqnmdb.im.domain.online.LoginUser;
import com.wqnmdb.im.exception.AllExceptionHandler;
import com.wqnmdb.im.service.UserCacheService;
import com.wqnmdb.im.service.impl.UserCacheServiceImpl;
import com.wqnmdb.im.utils.IPUtils;
import com.wqnmdb.im.utils.JwtUtils;
import com.wqnmdb.im.utils.SpringUtil;
import com.wqnmdb.im.utils.UserContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import java.util.HashSet;
import java.util.Set;

@Slf4j
public class SysTokenRealm extends AuthorizingRealm {

    /**
     * 此处必须懒加载，否则结果无法缓存
     */
    @Lazy
    @Autowired
    private UserCacheService userCacheService;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof ShiroToken;
    }

    @Override
    public String getName() {
        return AuthType.SYS_USER.toString();
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println(JSON.toJSONString(principalCollection));
        //这个里面存储的是主要身份信息,来源便是下面身份认证最后存入的第一个参数
        String primaryPrincipal = (String)principalCollection.getPrimaryPrincipal();
        log.info("权限:{}.", primaryPrincipal);
        //利用这些信息,便能从数据库或者其他位置获取权限列表了,具体实现略
        //获取用户的权限列表,存入set中
        Set<String> permsSet = new HashSet<>();
        permsSet.add(primaryPrincipal);

        //授权信息实例
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        //传入一个set
        info.setStringPermissions(permsSet);
        info.setRoles(permsSet);
        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //获取token
        ShiroToken shiroToken = (ShiroToken) authenticationToken;
        String token = shiroToken.getToken();
        Long sysUserId = null;
        try {
            sysUserId = JwtUtils.parseToken(token);
        } catch (Exception e) {
            log.error("token失效:{}.", token);
            throw new IncorrectCredentialsException("token失效");
        }
        //判断用户和token
        SysUser sysUser = userCacheService.getSysUserById(sysUserId);
        log.info("用户ID：{}.用户信息：{}.", sysUserId, JSON.toJSONString(sysUser));
        if (ObjectUtil.isNull(sysUser)){
            log.error("用户不存在,userId:{}.", sysUserId);
            throw new IncorrectCredentialsException("用户不存在");
        }
        if (!sysUser.getToken().equals(token)){
            log.error("token错误:{}.", sysUserId);
            throw new IncorrectCredentialsException("token错误");
        }
        // 将用户信息(具体类型看需求,可以是简单的string也可以是对象,要方便上面权限认证是容易获取用户信息), token 传入
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(SysRoleEnum.getKey(sysUser.getRole()), token, getName());
        setThreadUser(sysUser);
        return info;
    }

    /**
     * 设置UserContextUtil用户信息
     * @param sysUser
     */
    private void setThreadUser(SysUser sysUser){
        LoginUser loginUser = new LoginUser();
        BeanUtils.copyProperties(sysUser, loginUser);
        UserContextUtil.setUser(loginUser);
    }
}
