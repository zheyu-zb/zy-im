package com.wqnmdb.im.shiro;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.wqnmdb.im.domain.contants.SysRoleEnum;
import com.wqnmdb.im.domain.entity.ImUser;
import com.wqnmdb.im.domain.entity.SysUser;
import com.wqnmdb.im.domain.online.LoginUser;
import com.wqnmdb.im.service.UserCacheService;
import com.wqnmdb.im.utils.JwtUtils;
import com.wqnmdb.im.utils.UserContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import java.util.HashSet;
import java.util.Set;

@Slf4j
public class UserTokenRealm extends AuthorizingRealm {

    /**
     * 此处必须懒加载，否则结果无法缓存
     */
    @Lazy
    @Autowired
    private UserCacheService userCacheService;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof ShiroToken;
    }

    @Override
    public String getName() {
        return AuthType.APP_USER.toString();
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        log.info("0234234324234234324");
        //授权信息实例
        return new SimpleAuthorizationInfo();
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //获取token
        ShiroToken token = (ShiroToken) authenticationToken;

        //判断用户和token
        ImUser imUser = userCacheService.getAppUserByASKey(token.getAccessKey(), token.getSecretKey());
        log.info("IM_USER用户信息：{}.", JSON.toJSONString(imUser));
        if (ObjectUtil.isNull(imUser)){
            log.error("用户不存在,ak:{}.sk:{}.", token.getAccessKey(), token.getSecretKey());
            throw new IncorrectCredentialsException("用户不存在");
        }
        // 将用户信息(具体类型看需求,可以是简单的string也可以是对象,要方便上面权限认证是容易获取用户信息), token 传入
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo("im-user", token, getName());
        setThreadUser(imUser);
        return info;
    }

    /**
     * 设置UserContextUtil用户信息
     * @param imUser
     */
    private void setThreadUser(ImUser imUser){
        LoginUser loginUser = new LoginUser();
        BeanUtils.copyProperties(imUser, loginUser);
        UserContextUtil.setUser(loginUser);
    }
}
