package com.wqnmdb.im.redisson;

import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.redisson.codec.SerializationCodec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RedissonTopic {

    @Autowired
    private RedissonClient redissonClient;

    /**
     * 发布消息
     * @param name
     * @param message
     * @return
     */
    public Long publish(String name, Object message){
        RTopic topic = redissonClient.getTopic(name);
        return topic.publish(message);
    }

    /**
     * 获取主题
     * @param name
     * @return
     */
    public RTopic getTopic(String name){
        RTopic topic = redissonClient.getTopic(name);
//        topic.addListener(Car.class, new MessageListener<Car>() {
//            @Override
//            public void onMessage(CharSequence charSequence, Car car) {
//                System.out.println("onMessage:"+charSequence+"; Thread: "+Thread.currentThread().toString());
//                System.out.println(car.getColour()+" price : "+car.getPrice());
//            }
//        });
        return topic;
    }
}
