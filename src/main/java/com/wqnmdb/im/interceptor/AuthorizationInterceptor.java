package com.wqnmdb.im.interceptor;

import com.alibaba.fastjson.JSON;
import com.wqnmdb.im.domain.contants.SystemConstant;
import com.wqnmdb.im.domain.entity.ImUser;
import com.wqnmdb.im.domain.entity.SysUser;
import com.wqnmdb.im.domain.online.LoginUser;
import com.wqnmdb.im.service.UserService;
import com.wqnmdb.im.service.impl.UserCacheServiceImpl;
import com.wqnmdb.im.utils.IPUtils;
import com.wqnmdb.im.utils.JwtUtils;
import com.wqnmdb.im.utils.UserContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 信息验证
 */
@Slf4j
@Component
public class AuthorizationInterceptor implements HandlerInterceptor {

    @Autowired
    private UserCacheServiceImpl userCacheService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        UserContextUtil.getUser().setUserIp(IPUtils.getIpAddr(request));
        return Boolean.TRUE;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
       // del
        UserContextUtil.remove();
    }
}
