package com.wqnmdb.im.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wqnmdb.im.domain.entity.ImUser;

/**
 * @Entity generator.domain.ImUser
 */
public interface ImUserMapper extends BaseMapper<ImUser> {

}




