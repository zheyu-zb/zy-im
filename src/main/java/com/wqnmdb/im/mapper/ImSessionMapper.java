package com.wqnmdb.im.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wqnmdb.im.domain.entity.ImSession;

/**
 * @Entity generator.domain.ImSession
 */
public interface ImSessionMapper extends BaseMapper<ImSession> {

}




