package com.wqnmdb.im.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wqnmdb.im.domain.entity.ImMessage;

/**
 * @Entity generator.domain.ImMessage
 */
public interface ImMessageMapper extends BaseMapper<ImMessage> {

}




