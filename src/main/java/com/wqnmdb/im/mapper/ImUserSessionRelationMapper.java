package com.wqnmdb.im.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wqnmdb.im.domain.entity.ImUserSessionRelation;

/**
 * @Entity generator.domain.ImUserSessionRelation
 */
public interface ImUserSessionRelationMapper extends BaseMapper<ImUserSessionRelation> {

}




