package com.wqnmdb.im.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wqnmdb.im.domain.entity.ImApp;

/**
 * @Entity generator.domain.ImApp
 */
public interface ImAppMapper extends BaseMapper<ImApp> {

}




