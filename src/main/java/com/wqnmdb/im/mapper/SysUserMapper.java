package com.wqnmdb.im.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wqnmdb.im.domain.entity.SysUser;

/**
 * @Entity generator.domain.SysUser
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}




