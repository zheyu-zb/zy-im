package com.wqnmdb.im.exception;

import com.wqnmdb.im.domain.contants.ResponseEnum;
import com.wqnmdb.im.utils.result.R;
import com.wqnmdb.im.utils.result.RespUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.ShiroException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;

@Slf4j
@ControllerAdvice
public class AllExceptionHandler {

    /**
     * 如果抛出的的是ServiceException，则调用该方法
     */
    @ResponseBody
    @ExceptionHandler(ParamsException.class)
    public R<String> paramsExceptionHandle(ParamsException pe) {
        return RespUtils.error(pe.getCode(), pe.getMessage());
    }

    /**
     * 如果抛出的的是BusinessException，则调用该方法
     */
    @ResponseBody
    @ExceptionHandler(BusinessException.class)
    public R<String> businessExceptionHandle(BusinessException be) {
        return RespUtils.error(be.getCode(), be.getMessage());
    }

    /**
     * 如果抛出的的是UnauthorizedException，则调用该方法
     */
    @ResponseBody
    @ExceptionHandler(UnauthorizedException.class)
    public R<String> shiroExceptionHandle(UnauthorizedException ue) {
        log.error("权限不足", ue);
        return RespUtils.error(ResponseEnum.SHIRO_ROLE_ERR.getCode(), ResponseEnum.SHIRO_ROLE_ERR.getContent());
    }

    /**
     * 如果抛出的的是ServiceException，则调用该方法
     */
    @ResponseBody
    @ExceptionHandler(ServiceException.class)
    public R<String> serviceExceptionHandle(ServiceException se) {
        return RespUtils.error(se.getCode(), se.getMessage());
    }

    /**
     * 如果抛出的的是ServiceException，则调用该方法
     */
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public R<String> serviceExceptionHandle(Exception ex) {
        log.error("发生异常：{}.", ex.getMessage(), ex);
        return RespUtils.error(ResponseEnum.SERVER_ERR.getCode(), ex.getMessage());
    }
}
