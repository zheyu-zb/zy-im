package com.wqnmdb.im.exception;

import com.wqnmdb.im.domain.contants.ResponseEnum;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;


/**
 * 业务异常
 */
@Slf4j
@Getter
public class BusinessException extends RuntimeException{
    private Integer code = ResponseEnum.FAIL.getCode();

    /**
     * 自定义错误类型
     * @param msg 自定义的错误提示
     */
    public BusinessException(String msg){
        super(msg);
    }

    /**
     * 使用已有的错误类型
     * @param rEnum 枚举类中的错误类型
     */
    public BusinessException(ResponseEnum rEnum){
        super(rEnum.getContent());
        this.code = rEnum.getCode();
    }
}
