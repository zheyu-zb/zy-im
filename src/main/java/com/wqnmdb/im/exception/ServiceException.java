package com.wqnmdb.im.exception;

import com.wqnmdb.im.domain.contants.ResponseEnum;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;


/**
 * 参数异常
 */
@Slf4j
@Getter
public class ServiceException extends RuntimeException{
    private Integer code;

    /**
     * 使用已有的错误类型
     * @param type 枚举类中的错误类型
     */
    public ServiceException(ResponseEnum type, Exception ex){
        super(type.getContent());
        this.code = type.getCode();
        log.error("发生异常：" + ex.getMessage(), ex);
    }

    /**
     * 自定义错误类型
     * @param code 自定义的错误码
     * @param msg 自定义的错误提示
     */
    public ServiceException(Integer code, String msg){
        super(msg);
        this.code = code;
    }
}
