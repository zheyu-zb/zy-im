package com.wqnmdb.im.exception;

import com.wqnmdb.im.domain.contants.ResponseEnum;
import lombok.Getter;


/**
 * 参数异常
 */
@Getter
public class ParamsException extends RuntimeException{
    Integer code = ResponseEnum.INVALID_PARAMS.getCode();

    /**
     * 自定义错误类型
     * @param msg 自定义的错误提示
     */
    public ParamsException(String msg){
        super(msg);
    }
}
