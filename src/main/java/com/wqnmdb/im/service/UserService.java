package com.wqnmdb.im.service;

import com.wqnmdb.im.domain.entity.ImUser;
import com.wqnmdb.im.domain.resp.RespAppInfoModel;
import com.wqnmdb.im.domain.resp.RespUserInfoModel;
import com.wqnmdb.im.utils.result.R;

public interface UserService {

    /**
     * 获取应用信息
     * @return
     */
    R<RespAppInfoModel> getAppInfo();

    /**
     * 获取用户信息
     * @return
     */
    R<RespUserInfoModel> getUserInfo();

    /**
     * 获取用户信息
     * @param accessKey
     * @param secretKey
     * @param appCode
     * @return
     */
    ImUser checkUser(String accessKey, String secretKey, String appCode);

    /**
     * 获取用户
     * @param id
     * @return
     */
    ImUser getUserById(Long id);
}
