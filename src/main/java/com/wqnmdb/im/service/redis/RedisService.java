package com.wqnmdb.im.service.redis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * redis服务
 *
 * @author 王欣
 */
@Slf4j
@Service
public class RedisService {

    @Autowired
    private RedisTemplate redisTemplate;


    public boolean existKey(String key) {
        log.debug("existKey().params:" + key);
        return redisTemplate.hasKey(key);
    }

    public void delKey(String key) {
        log.debug("delKey().params:" + key);
        redisTemplate.delete(key);
    }

    public Set<String> keys(String prefix) {
        return redisTemplate.keys(prefix);
    }

    public void delKeyByPrefix(String prefix) {
        Set<String> keys = redisTemplate.keys(prefix);
        for (String key : keys) {
            log.debug("delKey().params:" + key);
            redisTemplate.delete(key);
        }
    }

    public boolean addString(String key, String value) {
        BoundValueOperations<String, String> ops = redisTemplate.boundValueOps(key);
        try {
            ops.set(value);
            return true;
        } catch (Exception e) {
            log.error("redis添加异常." + e.getMessage(), e);
            e.printStackTrace();
            return false;
        }
    }

    public boolean addString(String key, String value, long timeout, TimeUnit timeUnit) {
        BoundValueOperations<String, String> ops = redisTemplate.boundValueOps(key);
        try {
            ops.set(value, timeout, timeUnit);
            return true;
        } catch (Exception e) {
            log.error("redis添加异常." + e.getMessage(), e);
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 添加锁值
     */
    public boolean addStringIfAbsent(String key, String value, long timeout, TimeUnit timeUnit) {
        try {
            BoundValueOperations<String, String> ops = redisTemplate.boundValueOps(key);
            boolean result = ops.setIfAbsent(value);
            if (result) {
                //设置失效时间~
                redisTemplate.expire(key, timeout, timeUnit);
            }
            return result;
        } catch (Exception e) {
            log.error("redis添加锁值异常." + e.getMessage(), e);
            return false;
        }

    }

    public String getString(String key) {
        BoundValueOperations<String, String> ops = redisTemplate.boundValueOps(key);
        return ops.get();
    }

    public Long getStringExpire(String key) {
        BoundValueOperations<String, String> ops = redisTemplate.boundValueOps(key);
        return ops.getExpire();
    }

    public void setExpire(String key, Integer time) {
        redisTemplate.expire(key, time, TimeUnit.MILLISECONDS);
    }

    public Integer getExpire(String key) {
        Long expire = redisTemplate.getExpire(key);
        return expire.intValue();
    }

    public long incr(String key, Integer value) {
        return redisTemplate.opsForValue().increment(key, value);
    }
}
