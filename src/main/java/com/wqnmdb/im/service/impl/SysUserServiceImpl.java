package com.wqnmdb.im.service.impl;

import cn.hutool.crypto.digest.DigestUtil;
import com.wqnmdb.im.domain.contants.StatusEnum;
import com.wqnmdb.im.domain.contants.SysRoleEnum;
import com.wqnmdb.im.domain.contants.SystemConstant;
import com.wqnmdb.im.domain.entity.SysUser;
import com.wqnmdb.im.domain.online.TokenUserInfo;
import com.wqnmdb.im.domain.resp.RespSysUserLoginModel;
import com.wqnmdb.im.exception.BusinessException;
import com.wqnmdb.im.manager.SysUserManager;
import com.wqnmdb.im.service.SysUserService;
import com.wqnmdb.im.service.UserCacheService;
import com.wqnmdb.im.utils.CheckUtil;
import com.wqnmdb.im.utils.JwtUtils;
import com.wqnmdb.im.utils.result.R;
import com.wqnmdb.im.utils.result.RespUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.time.LocalDateTime;


/**
 * 消息服务
 */
@Slf4j
@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserManager sysUserManager;

    @Autowired
    private UserCacheService userCacheService;


    @Override
    public R<RespSysUserLoginModel> login(String account, String password) {
       // password = getPassword(password);
        SysUser sysUser = sysUserManager.lambdaQuery()
                .eq(SysUser::getAccount, account).eq(SysUser::getPassword, password)
                .eq(SysUser::getStatus, StatusEnum.VALID.getStatus()).last("LIMIT 1").one();
        CheckUtil.isNullException("账号或密码错误", sysUser);

        try {
            //创建token
            String token = JwtUtils.getToken(new TokenUserInfo(sysUser.getId()));

            //updare sysuser
            SysUser update = new SysUser();
            update.setId(sysUser.getId());
            update.setToken(token);
            update.setUpdateTime(LocalDateTime.now());
            sysUserManager.updateById(update);
            userCacheService.delSysUserCache(sysUser.getId());
            return RespUtils.success(new RespSysUserLoginModel(token));
        } catch (ParseException e) {
            log.error("生成token异常：", e);
            throw new BusinessException("生成签名失败");
        }
    }

    @Override
    public R<Boolean> addAccount(String account, String password, String nickname) {
        Long count = sysUserManager.lambdaQuery().eq(SysUser::getAccount, account).count();
        if (count > 0){
            throw new BusinessException("账号已存在");
        }

        SysUser sysUser = new SysUser();
        sysUser.setNickname(nickname);
        sysUser.setAccount(account);
        sysUser.setRole(SysRoleEnum.ADMIN.getRole());
        sysUser.setPassword(getPassword(password));
        sysUser.setSalt(SystemConstant.SYS_USER_SALT);
        sysUser.setCreateTime(LocalDateTime.now());
        sysUser.setUpdateTime(LocalDateTime.now());
        boolean bool = sysUserManager.save(sysUser);
        log.info("添加账号：{}.结果：{}.", account, bool);
        return RespUtils.success(bool);
    }

    /**
     * 获取密码
     * @param password
     * @return
     */
    private String getPassword(String password){
        return DigestUtil.md5Hex(password + SystemConstant.SYS_USER_SALT);
    }
}

