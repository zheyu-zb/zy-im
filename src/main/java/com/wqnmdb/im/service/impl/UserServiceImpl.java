package com.wqnmdb.im.service.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wqnmdb.im.domain.contants.MsgStatusEnum;
import com.wqnmdb.im.domain.contants.StatusEnum;
import com.wqnmdb.im.domain.entity.*;
import com.wqnmdb.im.domain.resp.RespAppInfoModel;
import com.wqnmdb.im.domain.resp.RespUserInfoModel;
import com.wqnmdb.im.exception.BusinessException;
import com.wqnmdb.im.manager.*;
import com.wqnmdb.im.service.MsgService;
import com.wqnmdb.im.service.UserService;
import com.wqnmdb.im.utils.CheckUtil;
import com.wqnmdb.im.utils.UserContextUtil;
import com.wqnmdb.im.utils.result.R;
import com.wqnmdb.im.utils.result.RespUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



/**
 * 消息服务
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private ImUserManager imUserManager;

    @Autowired
    private ImAppManager imAppManager;

    @Override
    public R<RespAppInfoModel> getAppInfo() {
        ImUser imUser = getUserById(UserContextUtil.getId());
        ImApp app = imAppManager.lambdaQuery().eq(ImApp::getAppCode, imUser.getAppCode()).eq(ImApp::getStatus, StatusEnum.VALID.getStatus()).one();

        RespAppInfoModel result = new RespAppInfoModel();
        BeanUtils.copyProperties(app, result);
        return RespUtils.success(result);
    }

    @Override
    public R<RespUserInfoModel> getUserInfo() {
        ImUser imUser = getUserById(UserContextUtil.getId());
        RespUserInfoModel result = new RespUserInfoModel();
        BeanUtils.copyProperties(imUser, result);
        return RespUtils.success(result);
    }

    /**
     * 校验用户
     */
    @Override
    public ImUser checkUser(String accessKey, String secretKey, String appCode) {
        LambdaQueryChainWrapper<ImUser> query = imUserManager.lambdaQuery().eq(ImUser::getAccessKey, accessKey).eq(ImUser::getSecretKey, secretKey);
        if (StrUtil.isNotBlank(appCode)) {
            query.eq(ImUser::getAppCode, appCode);
        }
        ImUser user = query.eq(ImUser::getStatus, StatusEnum.VALID.getStatus()).one();
        CheckUtil.isEmptyException("信息异常，检查未通过", user);
        return user;
    }

    /**
     * 获取用户
     * @param id
     * @return
     */
    @Override
    public ImUser getUserById(Long id) {
        ImUser user = imUserManager.getById(id);
        CheckUtil.isEmptyException("用户不存在", user);
        if (!user.getStatus().equals(StatusEnum.VALID.getStatus())){
            throw new BusinessException("用户状态异常");
        }
        return user;
    }
}

