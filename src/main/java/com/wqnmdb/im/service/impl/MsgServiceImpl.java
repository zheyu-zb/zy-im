package com.wqnmdb.im.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wqnmdb.im.domain.contants.MsgStatusEnum;
import com.wqnmdb.im.domain.contants.StatusEnum;
import com.wqnmdb.im.domain.entity.ImMessage;
import com.wqnmdb.im.domain.entity.ImSession;
import com.wqnmdb.im.domain.entity.ImUser;
import com.wqnmdb.im.domain.entity.ImUserSessionRelation;
import com.wqnmdb.im.exception.BusinessException;
import com.wqnmdb.im.manager.*;
import com.wqnmdb.im.service.MsgService;
import com.wqnmdb.im.service.UserService;
import com.wqnmdb.im.utils.CheckUtil;
import com.wqnmdb.im.utils.UserContextUtil;
import com.wqnmdb.im.utils.result.R;
import com.wqnmdb.im.utils.result.RespUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;


/**
 * 消息服务
 */
@Slf4j
@Service
public class MsgServiceImpl implements MsgService {

    @Autowired
    private ImSessionManager imSessionManager;

    @Autowired
    private ImMessageManager imMessageManager;

    @Autowired
    private ImUserSessionRelationManager imUserSessionRelationManager;


    @Override
    public R<PageInfo> getList(String appCode, Integer pageNum, Integer pageSize) {
        LambdaQueryWrapper<ImUserSessionRelation> query = new LambdaQueryWrapper<>();
        query.eq(ImUserSessionRelation::getUserId, UserContextUtil.getId());
        query.eq(ImUserSessionRelation::getStatus, StatusEnum.VALID.getStatus());
        //查询会话
        PageHelper.startPage(pageNum, pageSize);
        List<ImUserSessionRelation> list = imUserSessionRelationManager.list(query);
        return RespUtils.success(new PageInfo<>(list));
    }

    @Override
    public R<PageInfo> getListBySessionId(String sessionCode, String appCode, Integer pageNum, Integer pageSize) {
        ImSession session = imSessionManager.lambdaQuery()
                .eq(ImSession::getStatus, StatusEnum.VALID.getStatus()).eq(ImSession::getSessionCode, sessionCode).eq(ImSession::getAppCode, appCode).one();
        log.info("用户：{}.会话session信息：{}.", UserContextUtil.getId(), JSON.toJSONString(session));
        CheckUtil.isEmptyException("会话不存在", session);

        ImUserSessionRelation relation = imUserSessionRelationManager.lambdaQuery()
                .eq(ImUserSessionRelation::getSessionCode, sessionCode).eq(ImUserSessionRelation::getUserId, UserContextUtil.getId()).eq(ImUserSessionRelation::getStatus, StatusEnum.VALID.getStatus()).one();
        CheckUtil.isEmptyException("会话信息不存在", relation);

        LambdaQueryWrapper<ImMessage> query = new LambdaQueryWrapper<>();
        query.eq(ImMessage::getSessionCode, session.getSessionCode());
        query.between(ImMessage::getCreateTime, relation.getFirstMsgTime(), relation.getLastMsgTime());
        PageHelper.startPage(pageNum, pageSize);
        List<ImMessage> list = imMessageManager.list(query);

        if (list != null) {
            list.forEach(msg -> {
                if (msg.getStatus().equals(MsgStatusEnum.RECALL.getStatus())) {
                    msg.setMsgData("");
                }
            });
        }
        return RespUtils.success(new PageInfo<>(list));
    }

    @Override
    public R<String> delSessionById(String sessionCode, String appCode) {
        //查询会话
        ImSession session = imSessionManager.lambdaQuery()
                .eq(ImSession::getStatus, StatusEnum.VALID.getStatus()).eq(ImSession::getSessionCode, sessionCode).eq(ImSession::getAppCode, appCode).one();
        CheckUtil.isEmptyException("会话不存在", session);

        ImUserSessionRelation relation = imUserSessionRelationManager.lambdaQuery()
                .eq(ImUserSessionRelation::getSessionCode, sessionCode).eq(ImUserSessionRelation::getUserId, UserContextUtil.getId()).eq(ImUserSessionRelation::getStatus, StatusEnum.VALID.getStatus()).one();
        CheckUtil.isEmptyException("会话信息不存在", relation);

        ImUserSessionRelation update = new ImUserSessionRelation();
        update.setId(relation.getId());
        update.setStatus(StatusEnum.DELETE.getStatus());
        update.setFirstMsgTime(LocalDateTime.now());
        update.setLastMsgTime(LocalDateTime.now());
        update.setUpdateTime(LocalDateTime.now());
        boolean b1 = update.updateById();

        ImSession updateSession = new ImSession();
        updateSession.setId(relation.getId());
        updateSession.setUpdateTime(LocalDateTime.now());
        updateSession.setStatus(StatusEnum.DELETE.getStatus());
        boolean b2 = updateSession.updateById();
        if (b1 && b2) {
            return RespUtils.success("更新成功");
        } else {
            return RespUtils.success("更新失败");
        }
    }
}

