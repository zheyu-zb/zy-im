package com.wqnmdb.im.service.impl;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.wqnmdb.im.domain.contants.StatusEnum;
import com.wqnmdb.im.domain.contants.SystemConstant;
import com.wqnmdb.im.domain.entity.ImApp;
import com.wqnmdb.im.domain.entity.ImUser;
import com.wqnmdb.im.domain.online.LoginUser;
import com.wqnmdb.im.domain.resp.RespRegisterModel;
import com.wqnmdb.im.exception.BusinessException;
import com.wqnmdb.im.manager.ImAppManager;
import com.wqnmdb.im.manager.ImUserManager;
import com.wqnmdb.im.netty.data.InitNettyData;
import com.wqnmdb.im.service.RegisterService;
import com.wqnmdb.im.utils.*;
import com.wqnmdb.im.utils.result.R;
import com.wqnmdb.im.utils.result.RespUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


/**
 * 注册服务
 */
@Slf4j
@Service
public class RegisterServiceImpl implements RegisterService {

    @Autowired
    private ImAppManager imAppManager;

    @Autowired
    private ImUserManager imUserManager;

    @Autowired
    private InitNettyData initNettyData;


    @Override
    public R<RespRegisterModel> app(String appName) {
        //检查APP名称是否可用
        ImApp checkApp = imAppManager.lambdaQuery().eq(ImApp::getName, appName).eq(ImApp::getStatus, StatusEnum.VALID.getStatus()).one();
        if (checkApp != null){
            throw new BusinessException("应用名称重复");
        }

        //注册
        ImApp app = new ImApp();
        app.setName(appName);
        app.setStatus(StatusEnum.VALID.getStatus());
        app.setOnLineNum(0);
        app.setAppInfo("info");
        app.setAppCode(SysCodeUtil.getCode());
        app.setAccessKey(SysCodeUtil.getAccessKey());
        app.setSecretKey(SysCodeUtil.getSecretKey(app.getAppCode()));
        app.setUpdateTime(LocalDateTime.now());
        app.setCreateUser(UserContextUtil.getNickname());
        app.setUpdateUser(UserContextUtil.getNickname());
        app.setCreateTime(LocalDateTime.now());
        boolean save = app.insert();

        if (save){
            //初始化app通道容器
            initNettyData.createChannelContainer(appName);
            return RespUtils.success(new RespRegisterModel(app.getAccessKey(), app.getSecretKey()));
        }
        return RespUtils.fail("创建App失败");
    }

    @Override
    public R<RespRegisterModel> user(String accessKey, String secretKey, String os, Long appUserId, String nickname, String mobile) {
        if (appUserId <= 0){
            throw new BusinessException("缺少必要参数");
        }

        ImApp app = imAppManager.getAppByASKey(accessKey, secretKey, StatusEnum.VALID.getStatus());
        CheckUtil.isNullException("应用不存在", app);

        //check user
        Long count = imUserManager.lambdaQuery().eq(ImUser::getAppCode, app.getAppCode()).eq(ImUser::getAppUserId, appUserId).count();
        if (count > 0){
            throw new BusinessException("用户已存在");
        }

        RespRegisterModel result = null;
        try {
            //注册 id存在的话会抛异常
            ImUser user = new ImUser();
            user.setOs(os);
            user.setAppUserId(appUserId);
            user.setNickname(nickname);
            user.setMobile(mobile);
            user.setStatus(StatusEnum.VALID.getStatus());
            user.setIsOnLine(0);
            user.setAppId(app.getId());
            user.setSalt(SysCodeUtil.getUserSelt());
            user.setAppCode(app.getAppCode());
            user.setAccessKey(SysCodeUtil.getAccessKey());
            user.setSecretKey(SysCodeUtil.getSecretKey(app.getAppCode() + user.getSalt()));
            user.setCreateTime(LocalDateTime.now());
            user.setUpdateTime(LocalDateTime.now());
            boolean save = user.insert();
            if (save){
                result = new RespRegisterModel(user.getAccessKey(), user.getSecretKey());
            }
        }catch (Exception ex){
            throw new BusinessException("用户已存在");
        }
        return RespUtils.success(result);
    }
}

