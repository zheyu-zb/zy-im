package com.wqnmdb.im.service.impl;

import com.wqnmdb.im.domain.contants.RedisKeyConstant;
import com.wqnmdb.im.domain.contants.StatusEnum;
import com.wqnmdb.im.domain.contants.SystemConstant;
import com.wqnmdb.im.domain.entity.ImUser;
import com.wqnmdb.im.domain.entity.SysUser;
import com.wqnmdb.im.manager.ImUserManager;
import com.wqnmdb.im.manager.SysUserManager;
import com.wqnmdb.im.service.UserCacheService;
import com.wqnmdb.im.service.redis.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 字典配置表 服务实现类
 * </p>
 *
 * @author zy
 * @since 2021-05-22
 */
@Slf4j
@Service
@CacheConfig(cacheNames = {RedisKeyConstant.USER_CACHE_PROFIX})
public class UserCacheServiceImpl implements UserCacheService {

    @Autowired
    private SysUserManager sysUserManager;

    @Autowired
    private ImUserManager imUserManager;

    @Override
    @Cacheable(key = "':' + #p0", unless = "#result == null")
    public SysUser getSysUserById(Long sysUserId) {
        log.info("0000000000000000");
        SysUser sysUser = sysUserManager.getById(sysUserId);
        return sysUser;
    }

    @Override
    @Cacheable(key = "':' + #p0 + '+' + #p1", unless = "#result == null")
    public ImUser getAppUserByASKey(String accessKey, String secretKey) {
        log.info("11111111111111111");
        ImUser imUser = imUserManager.lambdaQuery().eq(ImUser::getAccessKey, accessKey)
                .eq(ImUser::getSecretKey, secretKey).eq(ImUser::getStatus, StatusEnum.VALID.getStatus()).one();
        return imUser;
    }

    @Override
    @CacheEvict(key="':' + #p0")
    public void delSysUserCache(Long sysUserId){
        log.info("删除系统用户缓存：{}.", sysUserId);
    }

    @Override
    @Cacheable(key = "':' + #p0 + '+' + #p1")
    public void delImUserCache(String accessKey, String secretKey) {
        log.info("删除APP用户缓存：{}.{}.", accessKey, secretKey);
    }
}
