package com.wqnmdb.im.service;


import com.wqnmdb.im.domain.resp.RespSysUserLoginModel;
import com.wqnmdb.im.utils.result.R;

/**
 * 系统用户相关
 */
public interface SysUserService {

    /**
     * 登陆
     * @param account
     * @param password
     * @return
     */
    R<RespSysUserLoginModel> login(String account, String password);

    /**
     * 添加账号
     * @param account
     * @param password
     * @param nickname
     * @return
     */
    R<Boolean> addAccount(String account, String password, String nickname);
}

