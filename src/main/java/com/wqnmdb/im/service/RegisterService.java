package com.wqnmdb.im.service;


import com.wqnmdb.im.domain.resp.RespRegisterModel;
import com.wqnmdb.im.utils.result.R;

public interface RegisterService {

    /**
     * 注册APP
     */
    R<RespRegisterModel> app(String appName);

    /**
     * 注册用户
     */
    R<RespRegisterModel> user(String accessKey, String secretKey, String os, Long appUserId, String nickname, String mobile);
}
