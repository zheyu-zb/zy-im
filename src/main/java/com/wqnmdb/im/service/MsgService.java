package com.wqnmdb.im.service;


import com.github.pagehelper.PageInfo;
import com.wqnmdb.im.utils.result.R;

import java.util.List;

public interface MsgService {

    /**
     * 获取会话列表
     */
    R<PageInfo> getList(String appCode, Integer pageNum, Integer pageSize);

    /**
     * 通过sessionId获取消息
     */
    R<PageInfo> getListBySessionId(String sessionCode, String appCode, Integer pageNum, Integer pageSize);

    /**
     * 删除会话
     */
    R<String> delSessionById(String sessionCode, String appCode);
}
