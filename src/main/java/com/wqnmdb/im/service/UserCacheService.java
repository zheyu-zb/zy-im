package com.wqnmdb.im.service;

import com.wqnmdb.im.domain.entity.ImUser;
import com.wqnmdb.im.domain.entity.SysUser;

/**
 * 用户缓存处理
 */
public interface UserCacheService {

    /**
     * 系统用户 会缓存
     * @param sysUserId
     * @return
     */
    SysUser getSysUserById(Long sysUserId);

    /**
     * 通过askey获取appUser
     * @param accessKey
     * @param secretKey
     * @return
     */
    ImUser getAppUserByASKey(String accessKey, String secretKey);

    /**
     * 删除缓存
     * @param sysUserId
     */
    void delSysUserCache(Long sysUserId);

    /**
     * 删除缓存
     * @param accessKey
     * @param secretKey
     */
    void delImUserCache(String accessKey, String secretKey);
}
