package com.wqnmdb.im;

import com.wqnmdb.im.domain.contants.SystemConstant;
import com.wqnmdb.im.netty.init.NettyServer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.net.InetSocketAddress;

@EnableAsync
@EnableCaching
@SpringBootApplication
@EnableTransactionManagement
@MapperScan(basePackages = {SystemConstant.MYBATIS_SCAN_BASE_PACKAGE})
public class ZyImApplication implements CommandLineRunner {

    @Value("${netty.port}")
    private int port;

    @Autowired
    private NettyServer nettyServer;

    public static void main(String[] args) {
        SpringApplication.run(ZyImApplication.class, args);
    }

    @Override
    public void run(String... args) {
        nettyServer.start(new InetSocketAddress(port));
    }
}
