package com.wqnmdb.im.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wqnmdb.im.mapper.ImMessageMapper;
import com.wqnmdb.im.domain.entity.ImMessage;
import com.wqnmdb.im.manager.ImMessageManager;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class ImMessageManagerImpl extends ServiceImpl<ImMessageMapper, ImMessage> implements ImMessageManager {

}




