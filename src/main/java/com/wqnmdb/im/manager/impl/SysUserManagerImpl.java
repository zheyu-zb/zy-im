package com.wqnmdb.im.manager.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wqnmdb.im.domain.entity.SysUser;
import com.wqnmdb.im.manager.SysUserManager;
import com.wqnmdb.im.mapper.SysUserMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class SysUserManagerImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserManager {

}




