package com.wqnmdb.im.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wqnmdb.im.domain.contants.StatusEnum;
import com.wqnmdb.im.domain.entity.ImApp;
import com.wqnmdb.im.mapper.ImUserMapper;
import com.wqnmdb.im.domain.entity.ImUser;
import com.wqnmdb.im.manager.ImUserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class ImUserManagerImpl extends ServiceImpl<ImUserMapper, ImUser> implements ImUserManager {
}




