package com.wqnmdb.im.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wqnmdb.im.mapper.ImSessionMapper;
import com.wqnmdb.im.domain.entity.ImSession;
import com.wqnmdb.im.manager.ImSessionManager;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class ImSessionManagerImpl extends ServiceImpl<ImSessionMapper, ImSession> implements ImSessionManager {

}




