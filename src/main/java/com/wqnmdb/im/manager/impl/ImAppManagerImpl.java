package com.wqnmdb.im.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wqnmdb.im.domain.contants.StatusEnum;
import com.wqnmdb.im.mapper.ImAppMapper;
import com.wqnmdb.im.domain.entity.ImApp;
import com.wqnmdb.im.manager.ImAppManager;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class ImAppManagerImpl extends ServiceImpl<ImAppMapper, ImApp> implements ImAppManager {
    @Override
    public ImApp getAppByASKey(String accessKey, String secretKey, Integer status) {
        return lambdaQuery()
                .eq(ImApp::getAccessKey, accessKey).eq(ImApp::getSecretKey, secretKey).eq(ImApp::getStatus, status).one();
    }
}




