package com.wqnmdb.im.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wqnmdb.im.mapper.ImUserSessionRelationMapper;
import com.wqnmdb.im.domain.entity.ImUserSessionRelation;
import com.wqnmdb.im.manager.ImUserSessionRelationManager;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class ImUserSessionRelationManagerImpl extends ServiceImpl<ImUserSessionRelationMapper, ImUserSessionRelation> implements ImUserSessionRelationManager {

}




