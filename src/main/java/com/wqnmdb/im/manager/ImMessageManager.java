package com.wqnmdb.im.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wqnmdb.im.domain.entity.ImMessage;

/**
 *
 */
public interface ImMessageManager extends IService<ImMessage> {

}
