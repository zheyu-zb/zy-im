package com.wqnmdb.im.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wqnmdb.im.domain.entity.SysUser;


/**
 *
 */
public interface SysUserManager extends IService<SysUser> {

}
