package com.wqnmdb.im.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wqnmdb.im.domain.entity.ImApp;
import com.wqnmdb.im.domain.entity.ImUser;

/**
 *
 */
public interface ImUserManager extends IService<ImUser> {
}
