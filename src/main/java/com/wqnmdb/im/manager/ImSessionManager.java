package com.wqnmdb.im.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wqnmdb.im.domain.entity.ImSession;

/**
 *
 */
public interface ImSessionManager extends IService<ImSession> {

}
