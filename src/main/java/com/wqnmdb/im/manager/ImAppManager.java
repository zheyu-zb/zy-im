package com.wqnmdb.im.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wqnmdb.im.domain.entity.ImApp;

/**
 *
 */
public interface ImAppManager extends IService<ImApp> {

    /**
     * 通过aKey&sKey获取app
     * @param accessKey
     * @param secretKey
     * @param status
     * @return
     */
    ImApp getAppByASKey(String accessKey, String secretKey, Integer status);
}
