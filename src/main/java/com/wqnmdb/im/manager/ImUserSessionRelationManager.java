package com.wqnmdb.im.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wqnmdb.im.domain.entity.ImUserSessionRelation;

/**
 *
 */
public interface ImUserSessionRelationManager extends IService<ImUserSessionRelation> {

}
