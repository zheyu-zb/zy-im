package com.wqnmdb.im.config;

import com.wqnmdb.im.domain.contants.SystemConstant;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2WebMvc
public class Knife4jConfig {

    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title("zy-im")
                        .description("zy-im系统接口文档说明")
                        .contact(new Contact("zy", "", "619675313@qq.com"))
                        .version("1.0")
                        .termsOfServiceUrl("")
                        .build())
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage(SystemConstant.SWAGGER_SCAN_BASE_PACKAGE))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(getGlobalParams());
        return docket;
    }

    /**
     * 获取请求头参数列表
     * @return
     */
    private List<Parameter> getGlobalParams(){
        ParameterBuilder tp1 = new ParameterBuilder();
        tp1.name(SystemConstant.AUTH_HEAD_NAME).description(SystemConstant.AUTH_HEAD_NAME)
                .modelRef(new ModelRef("string")).parameterType("header")
                //header中的ticket参数非必填，传空也可以
                .required(false);
        ParameterBuilder tp2 = new ParameterBuilder();
        tp2.name(SystemConstant.SK_HEAD_NAME).description(SystemConstant.SK_HEAD_NAME)
                .modelRef(new ModelRef("string")).parameterType("header")
                //header中的ticket参数非必填，传空也可以
                .required(false);
        ParameterBuilder tp3 = new ParameterBuilder();
        tp3.name(SystemConstant.AK_HEAD_NAME).description(SystemConstant.AK_HEAD_NAME)
                .modelRef(new ModelRef("string")).parameterType("header")
                //header中的ticket参数非必填，传空也可以
                .required(false);


        List<Parameter> pars = new ArrayList<Parameter>();
        pars.add(tp1.build());
        pars.add(tp2.build());
        pars.add(tp3.build());
        return pars;
    }
}