package com.wqnmdb.im.config;

import com.wqnmdb.im.interceptor.AuthorizationInterceptor;
import com.wqnmdb.im.interceptor.LogInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * classpath：默认只会在你项目的class路径中查找文件。
 * classpath*：默认不仅包含class路径，还包括jar文件中（class路径）进行查找。
 */
@Slf4j
@Configuration
public class WebAppConfig implements WebMvcConfigurer {

    @Autowired
    private LogInterceptor logInterceptor;

    @Autowired
    private AuthorizationInterceptor authorizationInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(logInterceptor).addPathPatterns("/**");
        // token 信息验证
        registry.addInterceptor(authorizationInterceptor).addPathPatterns("/zy/**").excludePathPatterns("/zy/sys/login");
    }


    /**
     * 跨域支持
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowCredentials(true)
                .allowedHeaders("*")
                .exposedHeaders("*")
                .allowedMethods("*")
                .allowedOriginPatterns("*")
                .maxAge(3600);
    }

}
