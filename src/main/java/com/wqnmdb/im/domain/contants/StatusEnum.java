package com.wqnmdb.im.domain.contants;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum StatusEnum {
    // 0-无效 1-有效 2-删除  invalid
    INVALID( 0, "无效"),
    VALID( 1, "有效"),
    DELETE( 2, "删除");


    private Integer status;
    private String content;

    /**
     * 通过code得到content
     */
    public static String getKey(Integer status){
        for(StatusEnum re : StatusEnum.values()){
            if(status.equals(re.getStatus())){
                return re.getContent();
            }
        }
        return "";
    }
}
