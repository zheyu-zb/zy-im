package com.wqnmdb.im.domain.contants;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum NettyCMDEnum {
    //Netty     //996-  1-ping  2-发消息  3-消息确认  4-撤回消息  96-创建sessionCode
    NETTY_AUTH( 996, "认证"),
    NETTY_HEARTBEAT( 1, "ping"),
    NETTY_SEND( 2, "发消息"),
    NETTY_AFFIRM( 3, "消息确认"),
    NETTY_RECALL( 4, "撤回消息"),
    NETTY_CREATE_SESSION( 96, "创建sessionCode"),
    NETTY_ERROR( 99, "消息异常"),
    NETTY_AUTH_TIMEOUT( 10, "netty定时任务时间（SECONDS）");


    private Integer code;
    private String content;

    /**
     * 通过code得到content
     */
    public static String getKey(Integer code){
        for(NettyCMDEnum re : NettyCMDEnum.values()){
            if(code.equals(re.getCode())){
                return re.getContent();
            }
        }
        return "";
    }
}
