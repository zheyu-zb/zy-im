package com.wqnmdb.im.domain.contants;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ResponseEnum {
    //响应状态
    SUCCESS( 5000, "成功"),
    FAIL( 1000, "失败"),
    ERROR( 2000, "error"),
    NOT_LOG_IN( 3000, "请登录"),
    SHIRO_ROLE_ERR( 3001, "权限不足"),
    TOKEN_ERR( 3002, "签名错误"),
    SERVER_ERR( 999, "系统繁忙"),
    INVALID_PARAMS( 1001, "参数错误"),
    INVALID_LOGIN( 1002, "登录失效");

    private Integer code;
    private String content;

    /**
     * 通过code得到content
     */
    public static String getKey(Integer code){
        for(ResponseEnum re : ResponseEnum.values()){
            if(code.equals(re.getCode())){
                return re.getContent();
            }
        }
        return "";
    }
}
