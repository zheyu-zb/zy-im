package com.wqnmdb.im.domain.contants;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum NettyServerStatusEnum {
    SUCCESS( 500, "成功"),
    FAIL( 100, "失败"),
    INVALID_PARAMS( 101, "参数错误"),
    SERVER_ERR( 999, "系统繁忙");


    private Integer code;
    private String content;

    /**
     * 通过code得到content
     */
    public static String getKey(Integer code){
        for(NettyServerStatusEnum re : NettyServerStatusEnum.values()){
            if(code.equals(re.getCode())){
                return re.getContent();
            }
        }
        return "";
    }
}
