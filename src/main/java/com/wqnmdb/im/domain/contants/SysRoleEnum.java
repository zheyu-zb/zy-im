package com.wqnmdb.im.domain.contants;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum SysRoleEnum {
    // 角色 0-未知 1-root 2-管理员
    UNKNOWN( 0, "未知"),
    ROOT( 1, "root"),
    ADMIN( 2, "管理员");


    private Integer role;
    private String content;

    /**
     * 通过code得到content
     */
    public static String getKey(Integer role){
        for(SysRoleEnum re : SysRoleEnum.values()){
            if(role.equals(re.getRole())){
                return re.getContent();
            }
        }
        return "";
    }
}
