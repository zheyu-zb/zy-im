package com.wqnmdb.im.domain.contants;

/**
 * @author lizhu
 * @version 1.0
 * @description: redis key
 * @date 2021/4/19 13:12
 */
public class RedisKeyConstant {

    /**
     * 缓存统一前缀
     */
    public static final String REDIS_PROFIX = "im:";

    /**
     * 缓存用户统一前缀
     */
    public static final String USER_CACHE_PROFIX = REDIS_PROFIX + "cache:user";

    /**
     * 缓存统一前缀
     */
    public static final String REDIS_BUSINESS_PROFIX = REDIS_PROFIX + "business";

    /**
     * 锁统一前缀
     */
    public static final String REDIS_LOCK_PROFIX = REDIS_PROFIX + "lock";

    /**
     * 广播统一前缀
     */
    public static final String REDIS_TOPIC_PROFIX = REDIS_PROFIX + "topic";

}
