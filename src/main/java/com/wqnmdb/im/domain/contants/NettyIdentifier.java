package com.wqnmdb.im.domain.contants;

import io.netty.util.AttributeKey;

/**
 * netty标识
 */
public class NettyIdentifier {

    /**
     * 是否登录
     */
    public static final AttributeKey<Boolean> AUTH_KEY = AttributeKey.valueOf("authentication");

    /**
     * 所属应用
     */
    public static final AttributeKey<String> APP_CODE = AttributeKey.valueOf("appCode");

    /**
     * 心跳超时次数上限
     */
    public static final AttributeKey<Integer> TIMEOUT_NUM = AttributeKey.valueOf("timeoutNum");

    /**
     * 是否清除
     */
    public static final AttributeKey<Boolean> CLEAR_KEY = AttributeKey.valueOf("clear");

    /**
     * 用户ID
     */
    public static final AttributeKey<Long> USER_ID_KEY = AttributeKey.valueOf("userId");
}
