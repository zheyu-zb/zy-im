package com.wqnmdb.im.domain.contants;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.digest.DigestUtil;

/**
 * @author lizhu
 * @version 1.0
 * @description: 系统常量
 * @date 2021/4/19 13:12
 */
public class SystemConstant {

    /**
     * 请求唯一ID
     */
    public static final String TRACE_ID = "traceId";

    /**
     * 系统生产code长度
     */
    public static final Integer SYS_CODE_LEN = 10;

    /**
     * AccessKey和SecretKey的长度
     */
    public static final Integer AS_KEY_LEN = 16;

    /**
     * 在线文档扫描路径
     */
    public static final String SWAGGER_SCAN_BASE_PACKAGE = "com.wqnmdb.im.controller";

    /**
     * 静态资源路径
     */
    public static final String MYBATIS_SCAN_BASE_PACKAGE = "com.wqnmdb.im.mapper";

    /**
     * http auth head
     */
    public static final String AUTH_HEAD_NAME = "AUTH";

    /**
     * http auth head
     */
    public static final String AK_HEAD_NAME = "AK";

    /**
     * http auth head
     */
    public static final String SK_HEAD_NAME = "SK";

    /**
     * system user salt
     */
    public static final String SYS_USER_SALT = "9q1khefw0ca2yaa7f5hwp8s4ldpsy06qvvkpn6ho2omaah4k5lftppzphq4fqqft";
}
