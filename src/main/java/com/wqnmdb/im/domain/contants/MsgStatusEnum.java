package com.wqnmdb.im.domain.contants;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum MsgStatusEnum {
    // 状态 -1已撤回 0-未送达  1-已送达  2-已读
    RECALL( -1, "已撤回"),
    UNDELIVERED( 0, "未送达"),
    ARRIVED( 1, "已送达"),
    READ( 1, "已读");


    private Integer status;
    private String content;

    /**
     * 通过code得到content
     */
    public static String getKey(Integer status){
        for(MsgStatusEnum re : MsgStatusEnum.values()){
            if(status.equals(re.getStatus())){
                return re.getContent();
            }
        }
        return "";
    }
}
