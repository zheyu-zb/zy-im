package com.wqnmdb.im.domain.req;

import com.wqnmdb.im.domain.req.base.ReqBaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
@ApiModel
public class ReqRegisterUserModel extends ReqBaseModel {

    @NotBlank(message = "accessKey必填")
    @ApiModelProperty(value = "accessKey", name = "accessKey", example = "5fa5023ba3c46a2292a25feb",position = 1)
    private String accessKey;

    @NotBlank(message = "secretKey必填")
    @ApiModelProperty(value = "secretKey", name = "secretKey", example = "MElzhGy8wdA=",position = 1)
    private String secretKey;

    @ApiModelProperty(value = "用户ID", name = "appUserId", example = "10001",position = 2)
    private Long appUserId = -1L;

    @ApiModelProperty(value = "昵称", name = "nickname", example = "小辣椒",position = 3)
    private String nickname;

    @ApiModelProperty(value = "手机号", name = "mobile", example = "13800138000",position = 4)
    private String mobile;
}
