package com.wqnmdb.im.domain.req;

import com.wqnmdb.im.domain.req.base.ReqPageModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
@ApiModel
public class ReqListMsgModel extends ReqPageModel {

    @NotBlank(message = "appCode必填")
    @ApiModelProperty(value = "appCode", name = "appCode", example = "1723665345",position = 3)
    private String appCode;
}
