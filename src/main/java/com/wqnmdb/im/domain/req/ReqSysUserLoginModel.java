package com.wqnmdb.im.domain.req;

import com.wqnmdb.im.domain.req.base.ReqBaseModel;
import com.wqnmdb.im.domain.req.base.ReqPageModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
@ApiModel
public class ReqSysUserLoginModel extends ReqBaseModel {

    @NotBlank(message = "账号必填")
    @ApiModelProperty(value = "account", name = "account", example = "996007",position = 1)
    private String account;

    @NotBlank(message = "password必填")
    @ApiModelProperty(value = "password", name = "password", example = "123321",position = 2)
    private String password;
}
