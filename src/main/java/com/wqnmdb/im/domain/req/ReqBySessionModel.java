package com.wqnmdb.im.domain.req;

import com.wqnmdb.im.domain.req.base.ReqPageModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
@ApiModel
public class ReqBySessionModel extends ReqPageModel {

    @NotBlank(message = "sessionCode必填")
    @ApiModelProperty(value = "sessionCode", name = "sessionCode", example = "5ed8b47fb964b97c9a4d9ee9",position = 3)
    private String sessionCode;

    @NotBlank(message = "appCode必填")
    @ApiModelProperty(value = "appCode", name = "appCode", example = "xiangkan",position = 4)
    private String appCode;
}
