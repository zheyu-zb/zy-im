package com.wqnmdb.im.domain.req.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;


@Data
@ApiModel("ReqPageModel")
public class ReqPageModel extends ReqBaseModel {

    @NotNull(message = "页码必填")
    @ApiModelProperty(value = "页码",example = "页码  int")
    private Integer pageNum;

    @NotNull(message = "页长必填")
    @ApiModelProperty(value = "页长",example = "页长  int")
    private Integer pageSize;

}
