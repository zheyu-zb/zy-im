package com.wqnmdb.im.domain.req;

import com.wqnmdb.im.domain.req.base.ReqBaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
@ApiModel
public class ReqRegisterAppModel extends ReqBaseModel {

    @NotBlank(message = "App名称必填")
    @ApiModelProperty(value = "App名称", name = "appName", example = "xiangkan",position = 2)
    private String appName;
}
