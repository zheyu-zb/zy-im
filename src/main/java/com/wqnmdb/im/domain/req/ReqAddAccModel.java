package com.wqnmdb.im.domain.req;

import com.wqnmdb.im.domain.req.base.ReqBaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;


@Data
@ApiModel
public class ReqAddAccModel extends ReqBaseModel {

    @NotBlank(message = "账号必填")
    @ApiModelProperty(value = "account", name = "account", example = "996007",position = 1)
    private String account;

    @NotBlank(message = "password必填")
    @Length(min = 8, max = 18, message = "密码长度错误")
    @ApiModelProperty(value = "password", name = "password", example = "123321",position = 2)
    private String password;

    @Length(max = 12, message = "昵称长度错误")
    @ApiModelProperty(value = "昵称", name = "昵称", example = "小辣椒",position = 3)
    private String nickname;
}
