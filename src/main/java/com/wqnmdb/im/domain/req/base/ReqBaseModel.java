package com.wqnmdb.im.domain.req.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @Package: com.wetran.codex.domain.model.req
 * @ClassName: ReqBaseModel
 * @Description: 请求鸡Model
 * @Author: 王欣
 * @Version: 1.0
 */
@Data
@ApiModel
public class ReqBaseModel {

    @ApiModelProperty(value = "版本号", name = "v", example = "1.0" ,position = 1001)
    protected String v;

    @ApiModelProperty(value = "终端类型", name = "os", example = "WEB" ,position = 1002)
    protected String os;

}
