package com.wqnmdb.im.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 * @TableName im_message
 */
@Data
@TableName(value ="im_message")
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="IM消息对象", description="IM消息对象")
public class ImMessage extends Model<ImMessage> {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * app代码
     */
    @TableField(value = "app_code")
    @ApiModelProperty(value = "app代码")
    private String appCode;

    /**
     * uuid
     */
    @TableField(value = "uuid")
    @ApiModelProperty(value = "uuid")
    private String uuid;

    /**
     * 会话代码
     */
    @TableField(value = "session_code")
    @ApiModelProperty(value = "会话代码")
    private String sessionCode;

    /**
     * 发送方
     */
    @TableField(value = "from_user_id")
    @ApiModelProperty(value = "发送方")
    private Long fromUserId;

    /**
     * 接收方
     */
    @TableField(value = "to_user_id")
    @ApiModelProperty(value = "接收方")
    private Long toUserId;

    /**
     * 1-文字 2-图片 3-语音 4-静态表情 5-动态表情 6-外链 7-视频
     */
    @TableField(value = "msg_type")
    @ApiModelProperty(value = "1-文字 2-图片 3-语音 4-静态表情 5-动态表情 6-外链 7-视频")
    private Integer msgType;

    /**
     * 消息内容
     */
    @TableField(value = "msg_data")
    @ApiModelProperty(value = "消息内容")
    private String msgData;

    /**
     * 状态 -1已撤回 0-未送达  1-已送达  2-已读
     */
    @TableField(value = "status")
    @ApiModelProperty(value = "状态 -1已撤回 0-未送达  1-已送达  2-已读")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @Override
    public Serializable pkVal() {
        return this.id;
    }
}