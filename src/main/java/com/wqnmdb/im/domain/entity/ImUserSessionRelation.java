package com.wqnmdb.im.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 * @TableName im_user_session_relation
 */
@Data
@TableName(value ="im_user_session_relation")
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="IM用户会话关系对象", description="IM用户会话关系对象")
public class ImUserSessionRelation extends Model<ImSession> {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /**
     * 会话代码
     */
    @TableField(value = "session_code")
    @ApiModelProperty(value = "会话代码")
    private String sessionCode;

    /**
     * 第一条消息时间
     */
    @TableField(value = "first_msg_time")
    @ApiModelProperty(value = "第一条消息时间")
    private LocalDateTime firstMsgTime;

    /**
     * 最后一条消息时间
     */
    @TableField(value = "last_msg_time")
    @ApiModelProperty(value = "最后一条消息时间")
    private LocalDateTime lastMsgTime;

    /**
     * 状态 0-无效 1-有效 2-删除
     */
    @TableField(value = "status")
    @TableLogic(value = "1", delval = "2")
    @ApiModelProperty(value = "状态 0-无效 1-有效 2-删除")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @Override
    public Serializable pkVal() {
        return this.id;
    }
}