package com.wqnmdb.im.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 * @TableName im_app
 */
@Data
@TableName(value ="im_app")
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="IM应用对象", description="IM应用对象")
public class ImApp extends Model<ImApp> {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * app名称
     */
    @TableField(value = "name")
    @ApiModelProperty(value = "app名称")
    private String name;

    /**
     * app代码
     */
    @TableField(value = "app_code")
    @ApiModelProperty(value = "app代码")
    private String appCode;

    /**
     * 说明
     */
    @TableField(value = "app_info")
    @ApiModelProperty(value = "说明")
    private String appInfo;

    /**
     * 存取键
     */
    @TableField(value = "access_key")
    @ApiModelProperty(value = "存取键")
    private String accessKey;

    /**
     * 密钥
     */
    @TableField(value = "secret_key")
    @ApiModelProperty(value = "密钥")
    private String secretKey;

    /**
     * 在线人数
     */
    @TableField(value = "on_line_num")
    @ApiModelProperty(value = "在线人数")
    private Integer onLineNum;

    /**
     * app总人数
     */
    @TableField(value = "total_num")
    @ApiModelProperty(value = "app总人数")
    private Integer totalNum;

    /**
     * 状态 0-无效 1-有效 2-删除
     */
    @TableField(value = "status")
    @TableLogic(value = "1", delval = "2")
    @ApiModelProperty(value = "状态 0-无效 1-有效 2-删除")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user")
    private String createUser;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_user")
    private String updateUser;

    @Override
    public Serializable pkVal() {
        return this.id;
    }
}