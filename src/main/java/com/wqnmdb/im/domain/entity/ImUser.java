package com.wqnmdb.im.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 * @TableName im_user
 */
@Data
@TableName(value ="im_user")
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="IM用户对象", description="IM用户对象")
public class ImUser extends Model<ImSession> {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "app_user_id")
    @ApiModelProperty(value = "app内userId")
    private Long appUserId;

    /**
     * 昵称
     */
    @TableField(value = "nickname")
    @ApiModelProperty(value = "昵称")
    private String nickname;

    /**
     * 手机号
     */
    @TableField(value = "mobile")
    @ApiModelProperty(value = "手机号")
    private String mobile;

    /**
     * 盐
     */
    @TableField(value = "salt")
    @ApiModelProperty(value = "盐")
    private String salt;

    /**
     * 密码
     */
    @TableField(value = "access_key")
    @ApiModelProperty(value = "密码")
    private String accessKey;

    /**
     * 授权
     */
    @TableField(value = "secret_key")
    @ApiModelProperty(value = "授权")
    private String secretKey;

    /**
     * appId
     */
    @TableField(value = "app_id")
    @ApiModelProperty(value = "appId")
    private Long appId;

    /**
     * app代码
     */
    @TableField(value = "app_code")
    @ApiModelProperty(value = "app代码")
    private String appCode;

    /**
     * 是否在线  0-不在 1-在
     */
    @TableField(value = "is_on_line")
    @ApiModelProperty(value = "是否在线  0-不在 1-在")
    private Integer isOnLine;

    /**
     * 状态 0-无效 1-有效 2-删除
     */
    @TableField(value = "status")
    @TableLogic(value = "1", delval = "2")
    @ApiModelProperty(value = "状态 0-无效 1-有效 2-删除")
    private Integer status;

    /**
     * 终端类型  1-安卓 2-ios 3-h5
     */
    @TableField(value = "os")
    @ApiModelProperty(value = "终端类型  1-安卓 2-ios 3-h5")
    private String os;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @Override
    public Serializable pkVal() {
        return this.id;
    }
}