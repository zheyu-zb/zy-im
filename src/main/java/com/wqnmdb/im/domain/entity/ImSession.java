package com.wqnmdb.im.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 * @TableName im_session
 */
@Data
@TableName(value ="im_session")
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="IM会话对象", description="IM会话对象")
public class ImSession extends Model<ImSession> {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * app代码
     */
    @TableField(value = "app_code")
    @ApiModelProperty(value = "app代码")
    private String appCode;

    /**
     * 会话代码
     */
    @TableField(value = "session_code")
    @ApiModelProperty(value = "会话代码")
    private String sessionCode;

    /**
     * 发起者
     */
    @TableField(value = "origin_user_id")
    @ApiModelProperty(value = "发起者")
    private Long originUserId;

    /**
     * 参与者  多个逗号分隔
     */
    @TableField(value = "partic_user_id")
    @ApiModelProperty(value = "参与者 多个逗号分隔")
    private String particUserId;

    /**
     * 会话类型：1-一对一  2-群聊
     */
    @TableField(value = "type")
    @ApiModelProperty(value = "会话类型：1-一对一  2-群聊")
    private Integer type;

    /**
     * 状态 0-无效 1-有效 2-删除
     */
    @TableField(value = "status")
    @TableLogic(value = "1", delval = "2")
    @ApiModelProperty(value = "状态 0-无效 1-有效 2-删除")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @Override
    public Serializable pkVal() {
        return this.id;
    }
}