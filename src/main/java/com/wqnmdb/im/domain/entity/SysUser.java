package com.wqnmdb.im.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 * @TableName sys_user
 */
@Data
@TableName(value ="sys_user")
@ApiModel(value="系统用户对象", description="系统用户对象")
public class SysUser extends Model<SysUser> {

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 角色 0-未知 1-root 2-管理员
     */
    @TableField(value = "role")
    @ApiModelProperty(value = "角色 0-未知 1-admin 2-管理员")
    private Integer role;

    /**
     * 盐
     */
    @TableField(value = "salt")
    @ApiModelProperty(value = "盐")
    private String salt;

    /**
     * token
     */
    @TableField(value = "token")
    @ApiModelProperty(value = "token")
    private String token;

    /**
     * 账号
     */
    @TableField(value = "account")
    @ApiModelProperty(value = "账号")
    private String account;

    /**
     * 密码
     */
    @TableField(value = "password")
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 昵称
     */
    @TableField(value = "nickname")
    @ApiModelProperty(value = "昵称")
    private String nickname;

    /**
     * 状态 0-无效 1-有效 2-删除
     */
    @TableField(value = "status")
    @ApiModelProperty(value = "状态 0-无效 1-有效 2-删除")
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
}