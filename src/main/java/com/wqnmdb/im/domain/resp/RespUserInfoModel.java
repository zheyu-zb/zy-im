package com.wqnmdb.im.domain.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@ApiModel
@NoArgsConstructor
@AllArgsConstructor
public class RespUserInfoModel {

    /**
     * 主键
     */
    @ApiModelProperty(value = "id", name = "id", example = "1",position = 1)
    private Long id;

    /**
     * 昵称
     */
    @ApiModelProperty(value = "nickname", name = "昵称", example = "大辣椒",position = 2)
    private String nickname;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "mobile", name = "手机号", example = "13800138000",position = 3)
    private String mobile;

    /**
     * app代码
     */
    @ApiModelProperty(value = "appCode", name = "app代码", example = "123432423",position = 4)
    private String appCode;

    /**
     * 是否在线  0-不在 1-在
     */
    @ApiModelProperty(value = "isOnLine", name = "是否在线：0-不在 1-在", example = "1",position = 5)
    private Integer isOnLine;

    /**
     * 终端类型
     */
    @ApiModelProperty(value = "os", name = "终端类型", example = "1",position = 6)
    private String os;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "createTime", name = "创建时间", example = "2021-12-12 12:12:12",position = 7)
    private LocalDateTime createTime;

}
