package com.wqnmdb.im.domain.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@ApiModel
@NoArgsConstructor
@AllArgsConstructor
public class RespAppInfoModel {
    /**
     * 主键
     */
    @ApiModelProperty(value = "id", name = "id", example = "1",position = 1)
    private Long id;

    /**
     * app名称
     */
    @ApiModelProperty(value = "name", name = "app名称", example = "xiangkan",position = 2)
    private String name;

    /**
     * app代码
     */
    @ApiModelProperty(value = "appCode", name = "app代码", example = "1232131",position = 3)
    private String appCode;

    /**
     * 在线人数
     */
    @ApiModelProperty(value = "onLineNum", name = "在线人数", example = "1",position = 4)
    private Integer onLineNum;

    /**
     * app总人数
     */
    @ApiModelProperty(value = "totalNum", name = "app总人数", example = "10",position = 5)
    private Integer totalNum;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "createTime", name = "创建时间", example = "2021-10-10 10:10:10",position = 6)
    private LocalDateTime createTime;

}
