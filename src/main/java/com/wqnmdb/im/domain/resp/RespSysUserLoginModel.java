package com.wqnmdb.im.domain.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@ApiModel
@NoArgsConstructor
@AllArgsConstructor
public class RespSysUserLoginModel {

    @ApiModelProperty(value = "token", name = "token", example = "asdfasdfasfadsfdasfdasf",position = 1)
    private String token;
}
