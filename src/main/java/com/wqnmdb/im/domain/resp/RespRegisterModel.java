package com.wqnmdb.im.domain.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@ApiModel
@NoArgsConstructor
@AllArgsConstructor
public class RespRegisterModel {

    @ApiModelProperty(value = "accessKey", name = "accessKey", example = "5fa5023ba3c46a2292a25feb",position = 1)
    private String accessKey;

    @ApiModelProperty(value = "secretKey", name = "secretKey", example = "MElzhGy8wdA=",position = 2)
    private String secretKey;
}
