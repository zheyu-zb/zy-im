package com.wqnmdb.im.domain.online;

import lombok.Data;

@Data
public class LoginUser {

    /**
     * id
     */
    private Long id;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 状态 0-无效 1-有效 2-删除
     */
    private Integer status;

    /**
     * 用户IP
     */
    private String userIp;
}
