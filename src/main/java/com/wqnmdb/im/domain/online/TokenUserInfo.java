package com.wqnmdb.im.domain.online;

import lombok.Data;

/**
 * token 用户信息
 */
@Data
public class TokenUserInfo {
    private Long userId;

    public TokenUserInfo(Long userId) {
        this.userId = userId;
    }

    public TokenUserInfo() {
    }
}
