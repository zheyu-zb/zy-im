package com.wqnmdb.im.annotation;

import cn.hutool.core.util.RandomUtil;
import com.wqnmdb.im.domain.contants.SystemConstant;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 请求及响应数据日志打印
 */
@Slf4j
@Aspect
@Order(1)
@Component
public class TraceAspect {

    @Pointcut("@annotation(com.wqnmdb.im.annotation.Trace)")
    public void print() {
    }

    /**
     * @param point
     */
    @Around("print()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        MDC.put(SystemConstant.TRACE_ID, RandomUtil.randomString(10));
        //执行方法
        Object object = point.proceed();
        MDC.remove(SystemConstant.TRACE_ID);
        return object;
    }

    @AfterThrowing("print()")
    public void afterThrowing()  {
        MDC.remove(SystemConstant.TRACE_ID);
    }
}