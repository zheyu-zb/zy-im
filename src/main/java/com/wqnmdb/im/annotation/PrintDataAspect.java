package com.wqnmdb.im.annotation;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 请求及响应数据日志打印
 */
@Slf4j
@Aspect
@Order(1)
@Component
public class PrintDataAspect {

    @Pointcut("@annotation(io.swagger.annotations.ApiOperation))")
    public void print() {
    }

    /**
     * @param point
     */
    @Around("print()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        //获取注解
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();

        //目标类、方法
        List<String> classNames = Arrays.asList(method.getDeclaringClass().getName().split("\\."));
        String className = classNames.get(classNames.size() - 1);
        String methodName = method.getName();
        List<Object> args = new ArrayList<>();

        Arrays.asList(point.getArgs()).forEach(arg -> {
            if (arg instanceof MultipartFile) {
                args.add("[文件]");
            } else if (arg instanceof List) {
                try {
                    List<MultipartFile> files = (List<MultipartFile>) arg;
                    args.add("[文件]");
                } catch (Exception e) {
                    args.add(arg);
                }
            } else if (arg instanceof HttpServletRequest) {
                args.add("Resquest");
            } else if (arg instanceof HttpServletResponse) {
                args.add("Response");
            } else {
                args.add(arg);
            }
        });
        try {
            log.info("类名：{}.方法名：{}.请求报文报文：{}.", className, methodName, JSON.toJSONString(args));
        } catch (Exception ex) {
        }

        //执行方法
        Object object = point.proceed();

        stopWatch.stop();
        try {
            log.info("类名：{}.方法名：{}.响应报文报文：{}.耗时: {}秒.", className, methodName, JSON.toJSONString(object), stopWatch.getTotalTimeSeconds());
        } catch (Exception ex) {
        }
        //整理施工现场
        return object;
    }
}