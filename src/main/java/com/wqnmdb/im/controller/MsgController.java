package com.wqnmdb.im.controller;

import com.github.pagehelper.PageInfo;
import com.wqnmdb.im.controller.base.BaseController;
import com.wqnmdb.im.domain.entity.SysUser;
import com.wqnmdb.im.domain.req.ReqBySessionModel;
import com.wqnmdb.im.domain.req.ReqDelSessionModel;
import com.wqnmdb.im.domain.req.ReqListMsgModel;
import com.wqnmdb.im.service.MsgService;
import com.wqnmdb.im.service.impl.UserCacheServiceImpl;
import com.wqnmdb.im.utils.CheckUtil;
import com.wqnmdb.im.utils.result.R;
import com.wqnmdb.im.utils.result.RespUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@Api(tags = "通行证相关")
@RequestMapping("/user/msg")
public class MsgController extends BaseController {

    @Autowired
    private MsgService msgService;

    @PostMapping("/getList")
    @ApiOperation(value = "获取会话", notes = "分页获取会话")
    public R<PageInfo> getList(@RequestBody ReqListMsgModel model) {
        //参数验证~
        CheckUtil.validator(model);
        R<PageInfo> result = msgService.getList(model.getAppCode(), model.getPageNum(), model.getPageSize());
        //返回结果~
        return result;
    }

    @PostMapping("/getListBySessionId")
    @ApiOperation(value = "获取消息", notes = "分页获取消息")
    public R<PageInfo> getListBySessionId(@RequestBody ReqBySessionModel model) {
        //参数验证~
        CheckUtil.validator(model);
        R<PageInfo> result = msgService.getListBySessionId(model.getSessionCode(), model.getAppCode(), model.getPageNum(), model.getPageSize());
        //返回结果~
        return result;
    }

    @PostMapping("/delSessionById")
    @ApiOperation(value = "删除会话", notes = "删除会话")
    public R<String> delSessionById(@RequestBody ReqDelSessionModel model) {
        //参数验证~
        CheckUtil.validator(model);
        R<String> result = msgService.delSessionById(model.getSessionCode(), model.getAppCode());
        //返回结果~
        return result;
    }
}
