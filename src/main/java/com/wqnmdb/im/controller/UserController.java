package com.wqnmdb.im.controller;

import com.wqnmdb.im.controller.base.BaseController;
import com.wqnmdb.im.domain.resp.RespAppInfoModel;
import com.wqnmdb.im.domain.resp.RespUserInfoModel;
import com.wqnmdb.im.service.UserService;
import com.wqnmdb.im.utils.result.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@Api(tags = "用户相关")
@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    @PostMapping("/getAppInfo")
    @ApiOperation(value = "获取应用信息", notes = "当前用户所在应用信息不包含敏感信息")
    public R<RespAppInfoModel> getAppInfo() {
        R<RespAppInfoModel> result = userService.getAppInfo();
        //返回结果~
        return result;
    }

    @PostMapping("/getUserInfo")
    @ApiOperation(value = "获取用户信息", notes = "当前用户信息不包含敏感信息")
    public R<RespUserInfoModel> getUserInfo() {
        R<RespUserInfoModel> result = userService.getUserInfo();
        //返回结果~
        return result;
    }
}
