package com.wqnmdb.im.controller;

import com.wqnmdb.im.controller.base.BaseController;
import com.wqnmdb.im.domain.req.ReqRegisterUserModel;
import com.wqnmdb.im.domain.req.ReqRegisterAppModel;
import com.wqnmdb.im.domain.resp.RespRegisterModel;
import com.wqnmdb.im.service.RegisterService;
import com.wqnmdb.im.utils.CheckUtil;
import com.wqnmdb.im.utils.result.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@Slf4j
@RestController
@RequestMapping("/sys/register")
@Api(value = "/sys/register", tags = {"注册相关"})
public class RegisterController extends BaseController {

    @Autowired
    private RegisterService registerService;

    @PostMapping("/app")
    @ApiOperation(value = "注册APP", notes = "令牌、APP名称必填")
    public R<RespRegisterModel> app(@RequestBody ReqRegisterAppModel model) {
        //参数验证~
        CheckUtil.validator(model);
        R<RespRegisterModel> result = registerService.app(model.getAppName());
        //返回结果~
        return result;
    }

    @PostMapping("/user")
    @ApiOperation(value = "注册用户", notes = "令牌、APP名称必填")
    public R<RespRegisterModel> user(@RequestBody ReqRegisterUserModel model) {
        //参数验证~
        CheckUtil.validator(model);
        R<RespRegisterModel> result = registerService.user(model.getAccessKey(),
                model.getSecretKey(), model.getOs(), model.getAppUserId(), model.getNickname(), model.getMobile());
        //返回结果~
        return result;
    }
}
