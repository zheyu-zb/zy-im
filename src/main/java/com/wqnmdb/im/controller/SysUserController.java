package com.wqnmdb.im.controller;

import com.wqnmdb.im.controller.base.BaseController;
import com.wqnmdb.im.domain.req.ReqAddAccModel;
import com.wqnmdb.im.domain.req.ReqSysUserLoginModel;
import com.wqnmdb.im.domain.resp.RespAppInfoModel;
import com.wqnmdb.im.domain.resp.RespSysUserLoginModel;
import com.wqnmdb.im.domain.resp.RespUserInfoModel;
import com.wqnmdb.im.service.SysUserService;
import com.wqnmdb.im.service.UserService;
import com.wqnmdb.im.utils.CheckUtil;
import com.wqnmdb.im.utils.result.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@Api(tags = "平台用户相关")
@RequestMapping("/sys/user")
public class SysUserController extends BaseController {

    @Autowired
    private SysUserService sysUserService;

    @PostMapping("/login")
    @ApiOperation(value = "登陆", notes = "系统登陆")
    public R<RespSysUserLoginModel> login(@RequestBody ReqSysUserLoginModel model) {
        CheckUtil.validator(model);
        R<RespSysUserLoginModel> result = sysUserService.login(model.getAccount(), model.getPassword());
        //返回结果~
        return result;
    }

    @PostMapping("/addAcc")
    @RequiresRoles({"root"})
    @ApiOperation(value = "添加账号", notes = "添加系统账号")
    public R<Boolean> addAccount(@RequestBody ReqAddAccModel model) {
        CheckUtil.validator(model);
        R<Boolean> result = sysUserService.addAccount(model.getAccount(), model.getPassword(), model.getNickname());
        //返回结果~
        return result;
    }
}
