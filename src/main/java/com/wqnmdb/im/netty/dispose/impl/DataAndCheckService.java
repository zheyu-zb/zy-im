package com.wqnmdb.im.netty.dispose.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.wqnmdb.im.domain.entity.ImMessage;
import com.wqnmdb.im.domain.entity.ImUser;
import com.wqnmdb.im.domain.protobuf.Message;
import com.wqnmdb.im.domain.protobuf.NettyModel;
import com.wqnmdb.im.netty.data.NettyData;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelId;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;


/**
 * 实时数据和检查服务
 */
@Slf4j
@Service
public class DataAndCheckService extends NettyData {

    /**
     * 检查用户及应用是否存在
     */
    public ImUser checkUserAuth(String accessKey, String secretKey, String appName) {

        return null;
    }

    /**
     * 检查是否是消息的接收方及msg是否是未读状态
     * 1-确认消息检查  2-撤回消息检查
     */
    public ImMessage checkMsg(Integer userId, String msgUuId, String appName, String sessionId, int cmd) {

        return null;
    }

    /**
     * 检查用户是否绑定
     */
    public boolean checkUserBinding(String appName, Long userId) {
        ConcurrentHashMap<String, ChannelId> userChannel = APP_BINDING_CHANNEL.get(appName);
        return userChannel.containsKey(userId.toString());
    }


    /**
     * 获取用户管道ID
     */
    public ChannelHandlerContext getUserChannel(Integer userId, String appName) {
        ConcurrentHashMap<ChannelId, ChannelHandlerContext> contextChannel = SYS_CONTEXT_CHANNEL.get(appName);
        ConcurrentHashMap<String, ChannelId> userChannel = APP_BINDING_CHANNEL.get(appName);
        ChannelId channelId = userChannel.get(userId.toString());
        return contextChannel.get(channelId);
    }

    /**
     * 创建拓展参数
     */
    public String getExtras(String uuid, int status, String sessionId){
        JSONObject extras = new JSONObject();
        if (StrUtil.isNotBlank(uuid)){
            extras.put("uuid", uuid);
        }
        if (StrUtil.isNotBlank(sessionId)){
            extras.put("sessionId", sessionId);
        }
        if (status != 0){
            extras.put("status", status);
        }
        return extras.toJSONString();
    }

    /**
     * 获取响应消息builder
     */
    public NettyModel.RespModel getRespModel(int type, int code, String extras, Message.MessageInfo.Builder msgInfo){
        NettyModel.RespModel.Builder respModel = NettyModel.RespModel.newBuilder();
        if (type > 0){
            respModel.setType(type);
        }
        if (code > 0){
            respModel.setCode(code);
        }
        if (StrUtil.isNotBlank(extras)){
            respModel.setExtras(extras);
        }
        if (msgInfo != null){
            respModel.setMsgInfo(msgInfo);
        }
     //   respModel.setTimestamp(DateUtil.getNowInt());
        return respModel.build();
    }
}


