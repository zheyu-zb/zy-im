package com.wqnmdb.im.netty.dispose;


import com.wqnmdb.im.domain.entity.ImUser;
import com.wqnmdb.im.domain.protobuf.NettyModel;
import io.netty.channel.ChannelHandlerContext;

public interface MsgDispose {

    void auth(ChannelHandlerContext ctx, ImUser user);

    void createSession(ChannelHandlerContext ctx, NettyModel.ReqModel reqModel, ImUser user);

    void pong(ChannelHandlerContext ctx);

    void singleSend(ChannelHandlerContext ctx, NettyModel.ReqModel reqModel, ImUser user);

    void affirm(ChannelHandlerContext ctx, NettyModel.ReqModel reqModel, ImUser user);

    void recall(ChannelHandlerContext ctx, NettyModel.ReqModel reqModel, ImUser user);

    void msgError(ChannelHandlerContext ctx, NettyModel.ReqModel reqModel);

    void sendResp(ChannelHandlerContext ctx, NettyModel.RespModel builder);
}
