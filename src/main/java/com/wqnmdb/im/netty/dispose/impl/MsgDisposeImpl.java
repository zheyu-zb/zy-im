package com.wqnmdb.im.netty.dispose.impl;

import com.alibaba.fastjson.JSONObject;
import com.wqnmdb.im.domain.contants.NettyCMDEnum;
import com.wqnmdb.im.domain.contants.NettyIdentifier;
import com.wqnmdb.im.domain.contants.NettyServerStatusEnum;
import com.wqnmdb.im.domain.entity.ImUser;
import com.wqnmdb.im.domain.protobuf.Message;
import com.wqnmdb.im.domain.protobuf.NettyModel;
import com.wqnmdb.im.netty.data.NettyData;
import com.wqnmdb.im.netty.dispose.MsgDispose;
import com.wqnmdb.im.utils.ThreadPoolUtil;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 处理消息
 * A <=> server <=> B
 * 正常情况下,server收到A的消息后推送给B,接着B收到消息后通知server,接着server再通知A.
 * A发送消息给B后,如果A在指定时间内没有收到server的消息通知,那么A就要超时重发.
 * A的超时重发可能会导致B收到重复的消息,所以B接受消息时要进行去重.
 */
@Slf4j
@Service
public class MsgDisposeImpl extends NettyData implements MsgDispose {

    @Autowired
    private DataAndCheckService dataAndCheckService;


    /**
     * 996-认证
     */
    @Override
    public void auth(ChannelHandlerContext ctx, ImUser user) {
        Channel channel = ctx.channel();

        channel.attr(NettyIdentifier.APP_CODE).set(user.getAppCode());
        channel.attr(NettyIdentifier.TIMEOUT_NUM).set(0);
        channel.attr(NettyIdentifier.AUTH_KEY).set(Boolean.TRUE);
        channel.attr(NettyIdentifier.USER_ID_KEY).set(user.getId());
        sendResp(ctx, dataAndCheckService.getRespModel(
                NettyCMDEnum.NETTY_AUTH.getCode(), NettyServerStatusEnum.SUCCESS.getCode(), "", null));

        Runnable affirmSend = () -> {
            //确认消息补发


        };

        Runnable reissueSend = () -> {

        };
        ThreadPoolUtil.execute(affirmSend);
        ThreadPoolUtil.execute(reissueSend);
    }

    /**
     * 96-创建sessionID
     */
    @Override
    public void createSession(ChannelHandlerContext ctx, NettyModel.ReqModel reqModel, ImUser user) {
        //创建会话

    }

    /**
     * 1-pong
     */
    @Override
    public void pong(ChannelHandlerContext ctx) {
        sendResp(ctx, dataAndCheckService.getRespModel(
                NettyCMDEnum.NETTY_HEARTBEAT.getCode(), NettyServerStatusEnum.SUCCESS.getCode(), "", null));
    }

    /**
     * 2-发消息 一对一
     */
    @Override
    public void singleSend(ChannelHandlerContext ctx, NettyModel.ReqModel reqModel, ImUser user) {
        //发送消息
        String appName = ctx.channel().attr(NettyIdentifier.APP_CODE).get();
        Message.MessageInfo msgInfo = reqModel.getMsgInfo();

    }

    /**
     * 3-消息确认
     */
    @Override
    public void affirm(ChannelHandlerContext ctx, NettyModel.ReqModel reqModel, ImUser user) {
        //消息确认
        String appName = ctx.channel().attr(NettyIdentifier.APP_CODE).get();

    }

    /**
     * 4-撤回消息
     */
    @Override
    public void recall(ChannelHandlerContext ctx, NettyModel.ReqModel reqModel, ImUser user) {
        //撤回消息
        String appName = ctx.channel().attr(NettyIdentifier.APP_CODE).get();

    }

    /**
     * 消息错误
     */
    @Override
    public void msgError(ChannelHandlerContext ctx, NettyModel.ReqModel reqModel) {
        JSONObject jo = new JSONObject();
        jo.put("error", "消息错误");
        jo.put("info", "发送消息指令请求数据异常");
        sendResp(ctx, dataAndCheckService.getRespModel(
                        NettyCMDEnum.NETTY_ERROR.getCode(), NettyServerStatusEnum.FAIL.getCode(), jo.toJSONString(), null));

    }

    /**
     * 发送响应
     */
    @Override
    public void sendResp(ChannelHandlerContext ctx, NettyModel.RespModel builder) {
        if (ctx == null || builder == null) {
            log.info("通道不存在或消息为空");
            return;
        }
        Channel channel = ctx.channel();
        String appName = channel.attr(NettyIdentifier.APP_CODE).get();
        Long userId = channel.attr(NettyIdentifier.USER_ID_KEY).get();
        log.info("回复应用：{}.用户ID：{}.通道：{}.消息：{}.", appName, userId, channel.id(), builder);
        //将客户端的信息直接返回写入ctx刷新缓存区
        ctx.writeAndFlush(builder);
    }
}
