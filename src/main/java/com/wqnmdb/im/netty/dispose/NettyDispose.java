package com.wqnmdb.im.netty.dispose;

import com.wqnmdb.im.domain.entity.ImUser;
import io.netty.channel.ChannelHandlerContext;

public interface NettyDispose {

    void addChannel(ChannelHandlerContext ctx);

    void bindingUserChannel(ImUser user, String appName, ChannelHandlerContext ctx);

    void receiveMsg(ChannelHandlerContext ctx, Object msg);

    void remove(ChannelHandlerContext ctx);
}
