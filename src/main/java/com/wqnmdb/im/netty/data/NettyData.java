package com.wqnmdb.im.netty.data;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelId;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * all channels
 */
public class NettyData {

    /**
     * 当前系统在线数量
     */
    public static AtomicInteger sysOnLineNum = new AtomicInteger();

    /**
     * 所有的管道对象  APP名称 --> {通道ID --> ctx}
     */
    public static final ConcurrentHashMap<String, ConcurrentHashMap<ChannelId, ChannelHandlerContext>> SYS_CONTEXT_CHANNEL = new ConcurrentHashMap<>();

    /**
     * 所有用户绑定信息  APP名称 --> {userId --> 通道ID}
     */
    public static final ConcurrentHashMap<String, ConcurrentHashMap<String, ChannelId>> APP_BINDING_CHANNEL = new ConcurrentHashMap<>();

    //private static final ConcurrentLinkedQueue SYS_AFFIRM_QUEUE = new ConcurrentLinkedQueue();
}
