package com.wqnmdb.im.netty.handler;

import com.wqnmdb.im.netty.dispose.MsgDispose;
import com.wqnmdb.im.netty.dispose.NettyDispose;
import com.wqnmdb.im.netty.dispose.impl.DataAndCheckService;
import com.wqnmdb.im.netty.dispose.impl.MsgDisposeImpl;
import com.wqnmdb.im.netty.dispose.impl.NettyDisposeImpl;
import com.wqnmdb.im.utils.SpringUtil;

public interface BaseHandler {

    default NettyDispose getNettyDispose(){
        return SpringUtil.getBean(NettyDisposeImpl.class);
    }

    default DataAndCheckService getDataAndCheckService(){
        return SpringUtil.getBean(DataAndCheckService.class);
    }

    default MsgDispose getMsgDispose(){
        return SpringUtil.getBean(MsgDisposeImpl.class);
    }
}
