package com.wqnmdb.im.utils;

import com.alibaba.fastjson.JSON;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * @author lizhu
 * @version 1.0
 * @description:
 * @date 2023/8/1 15:11
 */
@Slf4j
public class ClassLoaderUtil {

    public static ClassLoader getClassLoader(String url) {
        try {
            Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
           if (!method.isAccessible()) {
               method.setAccessible(true);
            }

            URLClassLoader classLoader = new URLClassLoader(new URL[]{}, ClassLoader.getSystemClassLoader());
            method.invoke(classLoader, new URL(url));
            return classLoader;
        } catch (Exception e) {
            log.error("getClassLoader-error", e);
            return null;
        }
    }


    @SneakyThrows
    public static void main(String[] args) {
        //指定路径
        ClassLoader classLoader = ClassLoaderUtil.getClassLoader("file:D:/code/tools/toJar-1.0-SNAPSHOT.jar");

        //指定类名
        Class<?> clazz = classLoader.loadClass("com.test.Test01");

        Object o = clazz.getDeclaredConstructor().newInstance();
        Method toTest = clazz.getMethod("toTest");

        System.out.println(toTest.invoke(o));

        for (Method method : clazz.getMethods()) {
            System.out.println(method.toString());
        }
    }
//
//    public static void main(String[] args){
//        //外部jar所在位置
//        String path = "file:/D:/code/java/project/zy-im/src/main/java/com/wqnmdb/im/utils/jar/xxl-job-admin-2.3.0.jar";
//        URLClassLoader urlClassLoader =null;
//        Class<?> MyTest = null;
//        try {
//            //通过URLClassLoader加载外部jar
//            urlClassLoader = new URLClassLoader(new URL[]{new URL(path)});
//            //System.out.println(JSON.toJSONString(urlClassLoader));
//
//            //获取外部jar里面的具体类对象
//            MyTest = urlClassLoader.loadClass("com.xxl.job.admin.XxlJobAdminApplication");
//            //创建对象实例
//            Object instance = MyTest.newInstance();
//            //获取实例当中的方法名为show，参数只有一个且类型为string的public方法
//            Method method = MyTest.getMethod("show", String.class);
//            //传入实例以及方法参数信息执行这个方法
//            Object ada = method.invoke(instance, "ada");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }finally {
//            //卸载关闭外部jar
//            try {
//                urlClassLoader.close();
//            } catch (Exception e) {
//                System.out.println("关闭外部jar失败："+e.getMessage());
//            }
//        }
//    }

}


