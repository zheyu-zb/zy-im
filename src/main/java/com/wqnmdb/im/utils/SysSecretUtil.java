package com.wqnmdb.im.utils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SysSecretUtil {

    /**
     * 获取加密的Secret
     * @param data
     * @param salt
     * @return
     */
    public static String getEncryptSecret(String data, String salt){
        String PBEStr = PBEUtil.encryptAndDecrypt(data, salt, salt, PBEUtil.ENCRYPT);
        log.info("PBEStr:{}.", PBEStr);
        return PBEStr;
    }

    /**
     * 获取加密的Secret
     * @param PBEStr
     * @param salt
     * @return
     */
    public static String getDecodeSecret(String PBEStr, String salt){
        String data = PBEUtil.encryptAndDecrypt(PBEStr, salt, salt, PBEUtil.DECODE);
        log.info("dataStr:{}.", data);
        return data;
    }
}
