package com.wqnmdb.im.utils.result;

import com.wqnmdb.im.domain.contants.ResponseEnum;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class R<T> implements Serializable {

    /**
     * 状态码
     */
    private int code = ResponseEnum.SUCCESS.getCode();

    /**
     * 消息
     */
    private String msg = ResponseEnum.SUCCESS.getContent();

    /**
     * 时间戳
     */
    private LocalDateTime timestamp = LocalDateTime.now();

    /**
     * 数据
     */
    private T data;

    public R() {
    }

    public R(T data) {
        this.data = data;
    }

    public R(String msg) {
        this.msg = msg;
    }

    public R(String msg, T data) {
        this.msg = msg;
        this.data = data;
    }

    public R(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public R(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public R(int code, String msg, LocalDateTime timestamp, T data) {
        this.code = code;
        this.msg = msg;
        this.timestamp = timestamp;
        this.data = data;
    }
}
