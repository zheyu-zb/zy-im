package com.wqnmdb.im.utils.result;


import com.wqnmdb.im.domain.contants.ResponseEnum;

import java.util.HashMap;


public class RespUtils {

    public static <T> R<T> success(T data) {
        return new R<T>(data);
    }

    /**
     * 返回成功~
     */
    public static <T> R<T> successMsg(String msg) {
        return new R<T>(msg, (T) new HashMap<>());
    }

    /**
     * 返回失败
     */
    public static <T> R<T> fail(String msg) {
        return new R<T>(ResponseEnum.FAIL.getCode(), msg, (T) new HashMap<>());
    }

    /**
     * 返回失败
     */
    public static <T> R<T> fail(int code, String msg) {
        return new R<T>(code, msg, (T) new HashMap<>());
    }

    /**
     * 返回异常
     */
    public static <T> R<T> error(String msg) {
        return new R<T>(ResponseEnum.ERROR.getCode(), msg, (T) new HashMap<>());
    }

    /**
     * 返回异常
     */
    public static <T> R<T> error(int code, String msg) {
        return new R<T>(code, msg, (T) new HashMap<>());
    }
}
