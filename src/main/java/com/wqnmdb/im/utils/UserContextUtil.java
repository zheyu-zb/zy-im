package com.wqnmdb.im.utils;

import com.wqnmdb.im.domain.online.LoginUser;

/**
 * ThreadLocal[用户信息]
 */
public class UserContextUtil {
    private static ThreadLocal<LoginUser> localUser = ThreadLocal.withInitial(LoginUser::new);

    /**
     * 设置登录用户
     *
     * @param user 登录用户
     */
    public static void setUser(LoginUser user) {
        localUser.set(user);
    }

    /**
     * 删除上下文、其实一般不用删除
     */
    public static void remove() {
        localUser.remove();
    }

    /**
     * 获取当前用户信息
     * @return
     */
    public static LoginUser getUser(){
        return localUser.get();
    }

    /**
     * 获取用户ID
     * @return
     */
    public static Long getId(){
        return getUser().getId();
    }

    /**
     * 获取用户昵称
     * @return
     */
    public static String getNickname(){
        return getUser().getNickname();
    }


    /**
     * 获取用户ip
     * @return
     */
    public static String getUserIp(){
        return getUser().getUserIp();
    }
}
