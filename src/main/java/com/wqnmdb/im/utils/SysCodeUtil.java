package com.wqnmdb.im.utils;

import cn.hutool.core.util.HexUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.wqnmdb.im.domain.contants.SystemConstant;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5加密工具类
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class SysCodeUtil {

    /**
     * 获取CODE
     *
     * @return
     */
    public static String getCode() {
        return RandomUtil.randomNumbers(SystemConstant.SYS_CODE_LEN);
    }

    /**
     * 获取AccessKey
     *
     * @return
     */
    public static String getAccessKey() {
        return RandomUtil.randomString(SystemConstant.AS_KEY_LEN);
    }

    /**
     * 通过CODE获取SecretKey
     *
     * @return
     */
    public static String getSecretKey(String code) {
        return DigestUtil.md5Hex16(code);
    }

    /**
     * 获取SELT
     *
     * @return
     */
    public static String getUserSelt() {
        return RandomUtil.randomString(SystemConstant.AS_KEY_LEN);
    }
}