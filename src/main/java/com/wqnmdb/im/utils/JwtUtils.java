package com.wqnmdb.im.utils;

import java.text.ParseException;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.wqnmdb.im.domain.online.TokenUserInfo;
import lombok.extern.slf4j.Slf4j;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.InvalidClaimException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;


@Slf4j
public class JwtUtils {

    private static final String secretKey = "ZW5rdGFXMHVaSE5oWmk0M09EazBNemN5T0RJNU1Ua2pJUT09";
    private static final String JKS = "jks";
    private static final String ISS = "zy_bb";
    private static final Integer expiresMin = 60 * 24;

    private JwtUtils() {
    }


    /**
     * 生成token
     *
     * @return
     * @throws ParseException
     */
    public static String getToken(TokenUserInfo userInfo) throws ParseException {
        Date now = new Date();
        //增加2分钟的过期时间，用于测试
        Date expiresDate = DateUtil.offsetMinute(DateUtil.date(), expiresMin);

        log.info("now===" + DateUtil.formatDateTime(now));
        log.info("expiresDate===" + DateUtil.formatDateTime(expiresDate));

        // 设置头部信息
        Map<String, Object> header = new HashMap<>(2);
        header.put("alg", "HS256");

        Algorithm algorithm = getAlgorithm();
        String token = JWT.create()
                .withClaim(JKS, userInfo.getUserId())
                .withIssuer(ISS)
                .withHeader(header)
                .withExpiresAt(expiresDate)
                .sign(algorithm);
        log.info("生成token成功:{}.", JSON.toJSONString(token));
        return token;
    }


    /**
     * 解析token，并返回User对象
     *
     * @param token
     * @return
     * @throws ParseException
     */
    public static Long parseToken(String token) throws RuntimeException {
        String msg = null;
        try {
            JWTVerifier verifier = JWT.require(getAlgorithm())
                    .withIssuer(ISS)
                    .build();

            DecodedJWT jwt = verifier.verify(token);
            Long userId = jwt.getClaim(JKS).asLong();
            log.info("token解析成功，userId:{}.", userId);
            return userId;
        } catch (JWTDecodeException de) {
            msg = "密钥格式不正确";
        } catch (SignatureVerificationException de) {
            msg = "密钥签证不正确";
        } catch (TokenExpiredException tee) {
            msg = "密钥已过期";
        } catch (InvalidClaimException ice) {
            msg = "非法密钥";
        } catch (JWTVerificationException jwte) {
            msg = "密钥解析错误";
        }

        log.error("token:{}.解析失败,原因：{}.", token, msg);
        throw new RuntimeException(msg);
    }


    /**
     * 获取自定义密钥
     *
     * @return
     */
    private static Algorithm getAlgorithm() {
        if (StrUtil.isBlank(secretKey)) {
            throw new RuntimeException("jwt配置的密钥不能为空");
        }
        byte[] decode = Base64.getDecoder().decode(secretKey);
        return Algorithm.HMAC256(decode);
    }
}