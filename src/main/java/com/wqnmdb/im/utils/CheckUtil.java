package com.wqnmdb.im.utils;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.wqnmdb.im.exception.BusinessException;
import com.wqnmdb.im.exception.ParamsException;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * 检查工具类
 *
 * @author xiashanhao
 * @date 2020-08-24 17:58
 */
@Slf4j
public class CheckUtil {

    private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();


    /**
     * 适用于 String Interger Long 类型 Collection Map
     *
     * @param message
     * @param values
     * @param <T>
     */
    public static <T> void isEmptyException(String message, T... values) {
        if (checkIsEmpty(values)) {
            throw new BusinessException(message);
        }
    }

    /**
     * 适用于 String Interger Long 类型 Collection Map
     *
     * @param message
     * @param values
     * @param <T>
     */
    public static <T> void isNullException(String message, T... values) {
        if (checkIsNull(values)) {
            throw new BusinessException(message);
        }
    }


    /**
     * 适用于 String Interger Long 类型 Collection Map
     *
     * @param values
     * @param <T>
     */
    public static <T> boolean isEmpty(T... values) {
        return checkIsEmpty(values);
    }

    private static <T> boolean checkIsEmpty(T... values) {
        if (values == null || values.length == 0) {
            return true;
        }
        for (T va : values) {
            if (va instanceof String) {
                if (StrUtil.isBlank((String) va)) {
                    return true;
                }
            } else if (va instanceof Collection) {
                if (CollectionUtil.isEmpty((Collection) va)) {
                    return true;
                }
            } else if (va instanceof Map) {
                if (MapUtil.isEmpty((Map) va)) {
                    return true;
                }
            } else if (va == null) {
                return true;
            }
        }
        return false;
    }

    private static <T> boolean checkIsNull(T... values) {
        if (values == null) {
            return true;
        }
        for (T va : values) {
            if (va == null) {
                return true;
            }
        }
        return false;
    }

    /**
     * 不等于true 抛出异常
     *
     * @param message
     * @param obj
     */
    public static void notTrueException(String message, boolean obj) {
        if (!obj) {
            throw new BusinessException(message);
        }
    }


    /**
     * 不等于false 抛出异常
     *
     * @param message
     * @param obj
     */
    public static void notFalseException(String message, boolean obj) {
        if (obj) {
            throw new BusinessException(message);
        }
    }

    /**
     * 校验@Validator注解
     *
     * @return
     */
    public static <T> void validator(T object) {
        String msg = doValidator(object);
        if (StrUtil.isNotBlank(msg)) {
            throw new ParamsException(msg);
        }
    }

    private static <T> String doValidator(T object) {
        CheckUtil.isEmptyException("参数对象不能为空", object);
        Set<ConstraintViolation<T>> validate = validator.validate(object);
        if (CollectionUtil.isNotEmpty(validate)) {
            StringBuilder message = new StringBuilder();
            for (ConstraintViolation<T> violation : validate) {
                message.append(",").append(violation.getMessage());
            }
            message = message.replace(0, 1, "");
            return message.toString();
        }
        return null;
    }
}
