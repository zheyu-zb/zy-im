# zy-im

#### 介绍
基于Netty的IM服务 支持http及多个应用

#### 软件架构
springboot2.3.4 + netty4.1.54 + redis + mongodb + protobuf

#### 使用说明
启动工程，文档地址http://localhost:8998/doc.html

1.http调用/register/app接口创建App，默认Auth为!#-im-LZ114.Demo!-#0202*001

2.拿到AK和SK后调用/register/user接口创建用户，注意，userId必须唯一

3.拿到AK和SK后客户端和服务端可建立长连接TCP server：localhost:8999

4.通道建立后第一个请求必须是认证，请求model如下：
   {cmd: 996
    appName: "xiangkan",
    accessKey: "UKr6XmbTKa4=",
    secretKey: "osuuCV54u10="}
    
    
    认证通过服务端响应model：
    {type: 996,
     code: 5000,
     timestamp: 1606792707}
     
5.认证通过后发送心跳model：

    {cmd: 1,
     timestamp: 1606792802}
     服务端响应model:
     {type: 1,
     code: 5000,
     timestamp: 1606792802}
     
6.一对一聊天，需建立sessionID后才能互发消息，代码具体参考com.wqnmdb.im.client.NettyClientHandler，数据模型参考com.wqnmdb.im.domain.netty.protobuf

7.认证通过后不需要再传appName，accessKey，secretKey

8.关于消息处理机制（com.wqnmdb.im.netty.dispose.impl.MsgDisposeImpl）:

          A <=> server <=> B
          
       1).正常情况下,server收到A的消息后推送给B,接着B收到消息后通知server,接着server再通知A.
       
       2).A发送消息给B后,如果A在指定时间内没有收到server的消息通知,那么A就要超时重发.
       
       3).A的超时重发可能会导致B收到重复的消息,所以B接受消息时要进行去重.
       
       4).A发消息给B，如果B没有在线，server会通知A这时客户端不用重发消息，B连接server后会主动将消息推送B.
       
       5).A发消息给B，B收到消息向A确认时A掉线，这时会将确认消息保存并通知B服务端已缓存，当A连接后会主动将消息推送A.
       
       
9.测试运行NettyClient即可，如不使用Minio，删除FileController、MinioService、MinIoProperties及配置即可

10.系统启动时会清理所有缓存并创建所有app的通道容器，如有统计需求请自行拓展NettyData

10.配置redis及mongodb相关地址~  其他业务请自行拓展，有任何疑问请添加微信：1607694304
       
    